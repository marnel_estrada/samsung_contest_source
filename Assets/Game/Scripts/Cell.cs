﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;
using Common.Utils;
using Common.Math;

namespace Game {
    public class Cell {

        // Modes
        public const int NONE = 0;
        public const int BLOCKER = 1;
        public const int HERO1 = 2;
        public const int TARGET1 = 3;
        public const int HERO2 = 4;
        public const int TARGET2 = 5;

        private IntVector3 position = new IntVector3();

        // Used for AStar
        private Cell parent;
        private float gScore;
        private float hScore;

        private readonly FlattenedCell flattenedCell;

        // This is distance from the starting cell
        private int distance;

        /// <summary>
        /// Constructor
        /// </summary>
        public Cell(FlattenedCell flattenedCell, int x, int y, int z) {
            this.flattenedCell = flattenedCell;
            this.position.Set(x, y, z);
        }

        /// <summary>
        /// Resets the state of the cell
        /// </summary>
        public void Reset() {
            this.parent = null;
            this.gScore = 0;
            this.hScore = 0;
            this.distance = int.MaxValue;
        }

        public bool IsBlocker {
            get {
                return this.flattenedCell.Mode == Cell.BLOCKER;
            }
        }

        public IntVector3 Position {
            get {
                return position;
            }
        }

        public float G {
            get {
                return gScore;
            }

            set {
                gScore = value;
            }
        }

        public float H {
            get {
                return hScore;
            }

            set {
                hScore = value;
            }
        }

        public float F {
            get {
                return this.gScore + this.hScore;
            }
        }

        public Cell Parent {
            get {
                return parent;
            }

            set {
                parent = value;
            }
        }

        /// <summary>
        /// Resets the AStar states
        /// </summary>
        public void ResetAStar() {
            parent = null;
            gScore = 0;
            hScore = 0;
        }

        public bool IsStartNode {
            get {
                return this.parent == null;
            }
        }

        public int x {
            get {
                return this.position.x;
            }
        }

        public int y {
            get {
                return this.position.y;
            }
        }

        public int z {
            get {
                return this.position.z;
            }
        }

        public FlattenedCell FlattenedCell {
            get {
                return flattenedCell;
            }
        }

        public int Distance {
            get {
                return distance;
            }

            set {
                distance = value;
            }
        }

    }
}
