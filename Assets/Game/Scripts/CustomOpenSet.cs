﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;

using ShadowStar.Utils;
using ShadowStar.AStar;

namespace Game {
    /// <summary>
    /// A customized open set
    /// </summary>
    public class CustomOpenSet {

        private BinaryHeap<Cell> heap = new BinaryHeap<Cell>(new CellHeapComparator());
        private HashSet<Cell> container = new HashSet<Cell>();

        /// <summary>
        /// Constructor
        /// </summary>
        public CustomOpenSet() {
        }

        /// <summary>
        /// Adds a cell
        /// </summary>
        /// <param name="cell"></param>
        public void Add(Cell cell) {
            this.heap.Add(cell);
            this.container.Add(cell);
        }

        /// <summary>
        /// Clears the open set
        /// </summary>
        public void Clear() {
            this.heap.Clear();
            this.container.Clear();
        }

        public bool IsEmpty {
            get {
                return this.heap.IsEmpty();
            }
        }

        /// <summary>
        /// Returns the cell with the least F score
        /// </summary>
        /// <returns></returns>
        public Cell GetCellWithLeastF() {
            return this.heap.RootValue;
        }

        /// <summary>
        /// Removes the cell with least F while also returning it
        /// </summary>
        /// <returns></returns>
        public Cell RemoveCellWithLeastF() {
            Cell removed = this.heap.RemoveRoot();
            this.container.Remove(removed);

            return removed;
        }

        /// <summary>
        /// Returns whether or not the open set already contains the specified cell
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public bool Contains(Cell cell) {
            return this.container.Contains(cell);
        }

        /// <summary>
        /// Fixes the ordering of the heap from the specified node. It's G value may been updated so we must restructure the heap.
        /// </summary>
        /// <param name="cell"></param>
        public void Fix(Cell cell) {
            this.heap.Fix(cell);
        }

    }
}
