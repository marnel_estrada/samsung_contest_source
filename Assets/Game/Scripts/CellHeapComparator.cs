﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;

using ShadowStar.Utils;

namespace Game {
    public class CellHeapComparator : BinaryHeapComparator<Cell> {

        public bool IsSameInstance(Cell a, Cell b) {
            return a == b;
        }

        public bool LesserThan(Cell a, Cell b) {
            return a.F < b.F;
        }

    }
}
