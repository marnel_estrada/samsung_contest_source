﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;

using ShadowStar.AStar;
using ShadowStar.Utils;

namespace Game {
    public class CustomAStar {

        private CellGrid cellGrid;

        private CustomOpenSet openSet = new CustomOpenSet();
        private HashSet<Cell> closeSet = new HashSet<Cell>();

        /// <summary>
        /// Constructor
        /// </summary>
        public CustomAStar(CellGrid cellGrid) {
            this.cellGrid = cellGrid;
        }

        /// <summary>
        /// Computes the heuristic cost
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goal"></param>
        /// <returns></returns>
        public static float ComputeHeuristicCost(Cell start, Cell goal) {
            return Mathf.Max(Mathf.Abs(goal.x - start.x), Mathf.Abs(goal.y - start.y));
        }

        private bool IsReachable(Cell cell) {
            // Reachable if not a blocker
            return !cell.IsBlocker;
        }

        private bool IsReachable(Cell from, Cell to, Hero otherHero) {
            if(to.IsBlocker) {
                // Destination is a blocker
                return false;
            }

            return otherHero.CanMove(from, to);
        }

        private static int OtherHero(int heroAgent) {
            if(heroAgent == 1) {
                return 2;
            }

            return 1;
        }

        /// <summary>
        /// Resolves the path from the starting cell to the goal cell
        /// </summary>
        /// <param name="path"></param>
        /// <param name="start"></param>
        /// <param name="goal"></param>
        public void ResolvePath(AStarPath<Cell> path, Cell start, Cell goal, Hero otherHero) {
            path.Clear();

            if(!IsReachable(goal)) {
                // Goal cell cannot be reached
                path.UnmarkAsReachable();
                return;
            }

            this.openSet.Clear();
            this.closeSet.Clear();

            start.Distance = 0;
            start.G = 0;
            start.H = ComputeHeuristicCost(start, goal);
            this.openSet.Add(start);

            while(!this.openSet.IsEmpty) {
                Cell current = this.openSet.GetCellWithLeastF();
                if(IsGoal(current, goal)) {
                    // Found the goal
                    ConstructPath(current, start, path);
                    path.MarkAsReachable();
                    return;
                }

                this.openSet.RemoveCellWithLeastF();
                this.closeSet.Add(current);

                ProcessCell(current, goal, otherHero);
            }

            // Unreachable
            path.UnmarkAsReachable();
        }

        // Note that we're only checking for x and y to see if we have reached the goal
        private bool IsGoal(Cell current, Cell goal) {
            return current.x == goal.x && current.y == goal.y;
        }

        private void ProcessCell(Cell current, Cell goal, Hero otherHero) {
            // Traverse neighbors
            int minX = current.x - 1;
            int minY = current.y - 1;
            int maxX = current.x + 1;
            int maxY = current.y + 1;

            for(int x = minX; x <= maxX; ++x) {
                for (int y = minY; y <= maxY; ++y) {
                    if(current.x == x && current.y == y) {
                        // Skip current position for now
                        continue;
                    }

                    Cell neighbor = this.cellGrid.GetCell(x, y, current.z);
                    if (neighbor != null) {
                        ProcessCell(current, neighbor, goal, otherHero);
                    }
                }
            }

            // Add the cell on the different z axis as neighbor
            if(current.z + 1 < CellGrid.Z_EXTENT) {
                Cell neighbor = this.cellGrid.GetCell(current.x, current.y, current.z + 1);
                ProcessCell(current, neighbor, goal, otherHero);
            }
        }

        private float ComputeDistance(Cell a, Cell b) {
            int diffX = Mathf.Abs(b.x - a.x);
            int diffY = Mathf.Abs(b.y - a.y);

            if(diffX > 0 && diffY > 0) {
                // Diagonal
                return 1.4142f;
            }

            return 1;
        }

        private void ProcessCell(Cell current, Cell neighbor, Cell goal, Hero otherHero) {
            if (!IsReachable(neighbor)) {
                // Not reachable
                return;
            }

            if (!IsReachable(current, neighbor, otherHero)) {
                // Not reachable
                return;
            }

            if (this.closeSet.Contains(neighbor)) {
                // Already in the closed set
                return;
            }

            // Distance to neighbor is always 1
            float tentativeG = current.G + ComputeDistance(current, neighbor);

            if(this.openSet.Contains(neighbor)) {
                // If an adjacent square is already on the open list, check to see if this path to that square is a better one.
                // In other words, check to see if the G score for that square is lower if we use the current square to get there. If not, don’t do anything.
                // On the other hand, if the G cost of the new path is lower, change the parent of the adjacent square to the selected square
                if(tentativeG < neighbor.G) {
                    // this is a better path, we change parent using the current node and assign the new lower G
                    neighbor.Parent = current;
                    neighbor.G = tentativeG;
                    this.openSet.Fix(neighbor);
                }
            } else {
                neighbor.Parent = current;
                neighbor.G = tentativeG;
                neighbor.H = ComputeHeuristicCost(neighbor, goal);
                this.openSet.Add(neighbor);
            }

            // Update the distance
            int distance = current.Distance + 1;
            if(distance < neighbor.Distance) {
                neighbor.Distance = distance;
            }
        }

        private void ConstructPath(Cell cell, Cell start, AStarPath<Cell> path) {
            // note here that we no longer need to reverse the ordering of the path
            // we just add them as reversed in AStarPath
            // AStarPath then knows how to handle this
            Cell current = cell;
            while (current != start) {
                path.Add(current);
                current = current.Parent;
            }
        }

    }
}
