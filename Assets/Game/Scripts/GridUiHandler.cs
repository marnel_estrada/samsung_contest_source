﻿using System.Collections;
using System.Collections.Generic;
using ShadowStar.AStar;
using UnityEngine;
using UnityEngine.UI;

namespace Game {
    public class GridUiHandler : MonoBehaviour {

        [SerializeField]
        private GridLayoutGroup gridLayout;

        [SerializeField]
        private GameObject cellPrefab;

        [SerializeField]
        private int maxWidth = 768;

        [SerializeField]
        private int gridSize = 10;

        private int currentMode = Cell.BLOCKER;

        private CellGrid grid;

        private Dictionary<FlattenedCell, CellView> viewMap = new Dictionary<FlattenedCell, CellView>();

        private CustomAStar aStar;
        private AStarPath<Cell> path = new AStarPath<Cell>();

        private void Awake() {
            Assertion.AssertNotNull(this.gridLayout);
            Assertion.AssertNotNull(this.cellPrefab);

            float cellSize = maxWidth / (float)gridSize;
            this.gridLayout.cellSize = new Vector2(cellSize, cellSize);

            this.gridLayout.constraintCount = gridSize;

            // Instantiate cells
            this.grid = new CellGrid(this.gridSize);
            int cellCount = this.gridSize * this.gridSize;
            this.grid.Prepare(cellCount);

            RectTransform root = this.GetComponent<RectTransform>();
            for(int i = 0; i < cellCount; ++i) {
                FlattenedCell cell = this.grid.Create(i);

                GameObject go = GameObject.Instantiate(this.cellPrefab);
                go.transform.SetParent(root);
                go.transform.localScale = VectorUtils.ONE;

                CellView view = go.GetComponent<CellView>();
                view.Init(cell, this);

                // Manage
                this.viewMap[cell] = view;
            }

            this.aStar = new CustomAStar(this.grid);
        }

        public int CurrentMode {
            get {
                return currentMode;
            }

            set {
                this.currentMode = value;
            }
        }

        /// <summary>
        /// Solves the problem
        /// </summary>
        public void Solve() {
            this.grid.ResetCells();

            Hero heroAgent1 = new Hero(1);

            FlattenedCell hero1 = this.grid.GetCellWithMode(Cell.HERO1);
            if (hero1 == null) {
                Debug.LogError("No Hero1");
                return;
            }
            // Add that start position as path so that the other hero will not come near
            heroAgent1.AddPath(this.grid.GetCell(hero1.x, hero1.y, 0));

            FlattenedCell target1 = this.grid.GetCellWithMode(Cell.TARGET1);
            if (target1 == null) {
                Debug.LogError("No Target1");
                return;
            }

            Hero heroAgent2 = new Hero(2);
            
            FlattenedCell hero2 = this.grid.GetCellWithMode(Cell.HERO2);
            if (hero2 == null) {
                Debug.LogError("No Hero2");
                return;
            }
            // Add that start position as path so that the other hero will not come near
            heroAgent2.AddPath(this.grid.GetCell(hero2.x, hero2.y, 0));

            FlattenedCell target2 = this.grid.GetCellWithMode(Cell.TARGET2);
            if (target2 == null) {
                Debug.LogError("No Target2");
                return;
            }

            SearchPath(hero1, target1, true, heroAgent1, heroAgent2);
            SearchPath(hero2, target2, false, heroAgent2, heroAgent1);

            Debug.Log("Hero 1");
            heroAgent1.PrintPath();

            Debug.Log("Hero 2");
            heroAgent2.PrintPath();
        }

        private void TestCellGrid() {
            for (int y = 1; y <= this.gridSize; ++y) {
                for (int x = 1; x <= this.gridSize; ++x) {
                    FlattenedCell cell = this.grid.GetFlattendCell(x, y);
                    int value = (cell.Mode == Cell.BLOCKER) ? 1 : 0;
                    Debug.LogFormat("({0}, {1}) - {2}", x, y, value);
                }
            }
        }

        private int SearchPath(FlattenedCell start, FlattenedCell goal, bool withMarking, Hero agent, Hero otherHero) {
            this.grid.ResetCellsDistance();

            Cell startCell = this.grid.GetCell(start.x, start.y, 0);
            Cell goalCell = this.grid.GetCell(goal.x, goal.y, 0);
            this.aStar.ResolvePath(this.path, startCell, goalCell, otherHero);

            if (this.path.Reachable) {
                // Mark the starting point
                //if (withMarking) {
                //    MarkNeighbors(start, hero, 0);
                //}

                this.path.RestartPath();

                int distance = 1;
                while(path.HasMorePath()) {
                    Cell cell = path.NextPath();
                    cell.FlattenedCell.SetDistance(distance, agent.Id);
                    agent.AddPath(cell);

                    //if (withMarking) {
                    //    MarkNeighbors(cell.FlattenedCell, hero, distance);
                    //}
                    ++distance;
                }

                return this.path.PathCount;
            }
            
            Debug.Log("Unreachable: " + agent.Id);
            return 0;
        }

        private void MarkNeighbors(FlattenedCell cell, int hero, int mark) {
            // Always replace the mark on the cell
            cell.SetMark(hero, mark);

            // Traverse neighbors
            int minX = cell.x - 1;
            int minY = cell.y - 1;
            int maxX = cell.x + 1;
            int maxY = cell.y + 1;

            for (int x = minX; x <= maxX; ++x) {
                for (int y = minY; y <= maxY; ++y) {
                    if (cell.x == x && cell.y == y) {
                        // Skip current position
                        continue;
                    }

                    FlattenedCell neighbor = this.grid.GetFlattendCell(x, y);
                    if (neighbor != null) {
                        MarkNeighbor(neighbor, hero, mark);
                    }
                }
            }
        }

        private void MarkNeighbor(FlattenedCell neighbor, int hero, int mark) {
            neighbor.SetMark(hero, mark);
        }

    }
}
