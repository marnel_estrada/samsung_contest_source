﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;

namespace Game {
    public class ToggleModeSetter : MonoBehaviour {

        [SerializeField]
        private GridUiHandler handler;

        // The mode to set
        [SerializeField]
        private int mode;

        private void Awake() {
            Assertion.AssertNotNull(this.handler);
        }

        public bool ToggleValue {
            set {
                if(value) {
                    this.handler.CurrentMode = this.mode;
                }
            }
        }

    }
}
