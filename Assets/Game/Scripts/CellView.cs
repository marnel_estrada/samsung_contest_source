﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.UI;

using Common;

namespace Game {
    public class CellView : MonoBehaviour, FlattenedCellObserver {

        [SerializeField]
        private Image background;

        [SerializeField]
        private Text distance1Label;

        [SerializeField]
        private Text distance2Label;

        [SerializeField]
        private Text mark1Label;

        [SerializeField]
        private Text mark2Label;

        [SerializeField]
        private Color target1Color = ColorUtils.BLUE;

        [SerializeField]
        private Color target2Color = ColorUtils.RED;

        private FlattenedCell cell;
        private GridUiHandler handler;

        private void Awake() {
            Assertion.AssertNotNull(this.background);
            Assertion.AssertNotNull(this.distance1Label);
            Assertion.AssertNotNull(this.distance2Label);
            Assertion.AssertNotNull(this.mark1Label);
            Assertion.AssertNotNull(this.mark2Label);

            // Hidden by default
            this.distance1Label.gameObject.Deactivate();
            this.distance2Label.gameObject.Deactivate();
            this.mark1Label.gameObject.Deactivate();
            this.mark2Label.gameObject.Deactivate();
        }

        // Initializer
        public void Init(FlattenedCell cell, GridUiHandler handler) {
            this.cell = cell;
            this.cell.AddObserver(this);

            this.handler = handler;
        }

        public void OnModeChanged(int mode) {
            // Change the color
            switch (mode) {
                case Cell.NONE:
                    this.background.color = ColorUtils.WHITE;
                    break;

                case Cell.BLOCKER:
                    this.background.color = ColorUtils.BLACK;
                    break;

                case Cell.HERO1:
                    this.background.color = ColorUtils.RED;
                    break;

                case Cell.TARGET1:
                    this.background.color = this.target1Color;
                    break;

                case Cell.HERO2:
                    this.background.color = ColorUtils.BLUE;
                    break;

                case Cell.TARGET2:
                    this.background.color = this.target2Color;
                    break;
            }
        }

        /// <summary>
        /// Toggles the cell
        /// </summary>
        public void Toggle() {
            this.cell.ToggleOn = !this.cell.ToggleOn;
            if(this.cell.ToggleOn) {
                this.cell.Mode = this.handler.CurrentMode;
            } else {
                this.cell.Mode = Cell.NONE;
            }
        }

        public void OnHero1DistanceChanged(int distance) {
            if(distance == 0) {
                // This is reset
                this.distance1Label.gameObject.Deactivate();
                return;
            }

            this.distance1Label.text = distance.ToString();
            this.distance1Label.gameObject.Activate();
        }

        public void OnHero2DistanceChanged(int distance) {
            if (distance == 0) {
                // This is reset
                this.distance2Label.gameObject.Deactivate();
                return;
            }

            this.distance2Label.text = distance.ToString();
            this.distance2Label.gameObject.Activate();
        }

        public void OnMark1Changed(int mark) {
            if(mark < 0) {
                // Reset
                this.mark1Label.gameObject.Deactivate();
            }

            this.mark1Label.text = mark.ToString();
            this.mark1Label.gameObject.Activate();
        }

        public void OnMark2Changed(int mark) {
            if (mark < 0) {
                // Reset
                this.mark2Label.gameObject.Deactivate();
            }

            this.mark2Label.text = mark.ToString();
            this.mark2Label.gameObject.Activate();
        }

    }
}
