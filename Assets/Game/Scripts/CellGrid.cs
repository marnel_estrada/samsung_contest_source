﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;
using Common.Math;
using Common.Utils;

namespace Game {
    /// <summary>
    /// Handles a grid of cells
    /// </summary>
    public class CellGrid {

        public const int Z_EXTENT = 5;

        private int dimension;

        private FlattenedCell[] flattenedCells;

        private SimpleList<Cell[]> cellsList = new SimpleList<Cell[]>();

        /// <summary>
        /// Constructor
        /// </summary>
        public CellGrid(int dimension) {
            this.dimension = dimension;
        }

        /// <summary>
        /// Prepares the specified number of cells
        /// </summary>
        /// <param name="cellCount"></param>
        public void Prepare(int cellCount) {
            this.flattenedCells = new FlattenedCell[cellCount];

            for(int i = 0; i < Z_EXTENT; ++i) {
                this.cellsList.Add(new Cell[cellCount]);
            }
        }

        /// <summary>
        /// Resets all the cells
        /// </summary>
        public void ResetCells() {
            // Reset flattened cells
            for(int i = 0; i < this.flattenedCells.Length; ++i) {
                this.flattenedCells[i].Reset();
            }

            for (int i = 0; i < Z_EXTENT; ++i) {
                Cell[] cells = this.cellsList[i];
                ResetCells(cells);
            }
        }

        private static void ResetCells(Cell[] cells) {
            for (int i = 0; i < cells.Length; ++i) {
                cells[i].Reset();
            }
        }

        /// <summary>
        /// Resets the distance set to cells
        /// </summary>
        public void ResetCellsDistance() {
            for (int i = 0; i < Z_EXTENT; ++i) {
                Cell[] cells = this.cellsList[i];
                ResetCellsDistance(cells);
            }
        }

        private static void ResetCellsDistance(Cell[] cells) {
            for (int i = 0; i < cells.Length; ++i) {
                cells[i].Distance = int.MaxValue;
            }
        }

        /// <summary>
        /// Creates a cell at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public FlattenedCell Create(int index) {
            // We add one because the coordinates is one based
            // Note that we are breaking the coordinates from their example
            // x is column, and y is row
            int x = (index % dimension) + 1;
            int y = (index / dimension) + 1;
            FlattenedCell flatCell = new FlattenedCell(x, y);
            this.flattenedCells[index] = flatCell;

            // Add a cell to each dimension
            for(int z = 0; z < Z_EXTENT; ++z) {
                Cell cell = new Cell(flatCell, x, y, z);
                Cell[] cells = this.cellsList[z];
                cells[index] = cell;
            }

            return flatCell;
        }

        /// <summary>
        /// Returns the cell at the specified coordinate
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public FlattenedCell GetFlattendCell(int x, int y) {
            if(x <= 0 || x > this.dimension) {
                // Outside of map
                return null;
            }

            if(y <= 0 || y > this.dimension) {
                // Outside of map
                return null;
            }

            // We minus 1 because the specified coordinates are one based
            int index = ((y - 1) * this.dimension) + (x - 1);
            return this.flattenedCells[index];
        }

        /// <summary>
        /// Returns the cell at the specified coordinate
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public Cell GetCell(int x, int y, int z) {
            if (x <= 0 || x > this.dimension) {
                // Outside of map
                return null;
            }

            if (y <= 0 || y > this.dimension) {
                // Outside of map
                return null;
            }

            int index = ((y - 1) * this.dimension) + (x - 1);
            return this.cellsList[z][index];
        }

        /// <summary>
        /// Returns the cell with the specified mode
        /// Returns null if no cell with such mode is found
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        public FlattenedCell GetCellWithMode(int mode) {
            for (int i = 0; i < this.flattenedCells.Length; ++i) {
                if (this.flattenedCells[i].Mode == mode) {
                    return this.flattenedCells[i];
                }
            }

            return null;
        }

    }
}
