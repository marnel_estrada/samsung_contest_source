﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;
using Common.Utils;

namespace Game {
    /// <summary>
    /// Encapsulates a hero
    /// </summary>
    public class Hero {

        private int id;
        private SimpleList<Cell> path = new Common.Utils.SimpleList<Cell>();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        public Hero(int id) {
            this.id = id;
        }

        public int Id {
            get {
                return id;
            }
        }

        /// <summary>
        /// Clears the path
        /// </summary>
        public void ClearPath() {
            this.path.Clear();
        }

        /// <summary>
        /// Adds a path
        /// </summary>
        /// <param name="cell"></param>
        public void AddPath(Cell cell) {
            this.path.Add(cell);
        }

        /// <summary>
        /// Returns whether or not another agent can move from the specified starting point and destination
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public bool CanMove(Cell from, Cell to) {
            // We add one here because we are looking ahead
            int index = from.Distance + 1;

            Cell pathCell = null;
            if(index >= this.path.Count) {
                // Already past the path count
                // Use the last one
                return true;
            }

            pathCell = this.path[index];

            // The path should be greater than one to be able to move
            return CustomAStar.ComputeHeuristicCost(to, pathCell) > 1;
        }

        /// <summary>
        /// Prints the path
        /// </summary>
        public void PrintPath() {
            for(int i = 0; i < this.path.Count; ++i) {
                Debug.LogFormat("{0} - {1}", i, this.path[i].Position);
            }
        }

    }
}
