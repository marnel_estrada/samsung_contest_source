﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;

namespace Game {
    /// <summary>
    /// A 3 dimensional vector of integers.
    /// </summary>
    public class IntVector3 {

        public int x;
        public int y;
        public int z;

        /// <summary>
        /// Default constructor
        /// </summary>
        public IntVector3() : this(0, 0, 0) {
        }

        /// <summary>
        /// Constructor with coordinates
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public IntVector3(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Copies the values from the other vector
        /// </summary>
        /// <param name="other"></param>
        public void Set(IntVector3 other) {
            this.x = other.x;
            this.y = other.y;
            this.z = other.z;
        }

        /// <summary>
        /// Sets the coordinates
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Set(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public override string ToString() {
            return "({0}, {1}, {2})".FormatWith(this.x, this.y, this.z);
        }

    }
}
