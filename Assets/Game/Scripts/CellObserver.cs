﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;

namespace Game {
    public interface FlattenedCellObserver {

        void OnModeChanged(int mode);

        void OnHero1DistanceChanged(int distance);

        void OnHero2DistanceChanged(int distance);

        void OnMark1Changed(int mark);

        void OnMark2Changed(int mark);

    }
}
