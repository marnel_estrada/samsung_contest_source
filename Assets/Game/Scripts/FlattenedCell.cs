﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;
using Common.Utils;
using Common.Math;

namespace Game {
    /// <summary>
    /// This is the 2D cell that is used for views
    /// </summary>
    public class FlattenedCell {

        private int mode = 0;

        private int hero1Distance;
        private int hero2Distance;
        
        private int mark1 = -1;
        private int mark2 = -1;

        private bool toggleOn = false;

        private SimpleList<FlattenedCellObserver> observers = new SimpleList<FlattenedCellObserver>();

        private IntVector2 position = new IntVector2();

        /// <summary>
        /// Constructor with coordinates
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public FlattenedCell(int x, int y) {
            this.position.Set(x, y);
        }

        /// <summary>
        /// Resets the cell
        /// </summary>
        public void Reset() {
            this.Hero1Distance = 0;
            this.Hero2Distance = 0;
            this.Mark1 = -1;
            this.Mark2 = -1;
        }

        public int Mode {
            get {
                return this.mode;
            }

            set {
                this.mode = value;

                // Notify observers
                for (int i = 0; i < this.observers.Count; ++i) {
                    this.observers[i].OnModeChanged(this.mode);
                }
            }
        }

        public int Hero1Distance {
            get {
                return hero1Distance;
            }

            set {
                hero1Distance = value;

                // Notify observers
                for (int i = 0; i < this.observers.Count; ++i) {
                    this.observers[i].OnHero1DistanceChanged(this.hero1Distance);
                }
            }
        }

        public int Hero2Distance {
            get {
                return hero2Distance;
            }

            set {
                hero2Distance = value;

                // Notify observers
                for (int i = 0; i < this.observers.Count; ++i) {
                    this.observers[i].OnHero2DistanceChanged(this.hero2Distance);
                }
            }
        }

        private int Mark1 {
            get {
                return mark1;
            }

            set {
                // Set only if higher
                if (value > this.mark1) {
                    mark1 = value;

                    // Notify observers
                    for (int i = 0; i < this.observers.Count; ++i) {
                        this.observers[i].OnMark1Changed(this.mark1);
                    }
                }
            }
        }

        private int Mark2 {
            get {
                return mark2;
            }

            set {
                if (value > this.mark2) {
                    mark2 = value;

                    // Notify observers
                    for (int i = 0; i < this.observers.Count; ++i) {
                        this.observers[i].OnMark2Changed(this.mark2);
                    }
                }
            }
        }

        /// <summary>
        /// Sets the mark for the specified hero
        /// </summary>
        /// <param name="hero"></param>
        /// <param name="mark"></param>
        public void SetMark(int hero, int mark) {
            switch (hero) {
                case 1:
                    this.Mark1 = mark;
                    break;

                case 2:
                    this.Mark2 = mark;
                    break;
            }
        }

        /// <summary>
        /// Adds an observer
        /// </summary>
        /// <param name="observer"></param>
        public void AddObserver(FlattenedCellObserver observer) {
            this.observers.Add(observer);
        }

        /// <summary>
        /// Sets the distance
        /// </summary>
        /// <param name="distance"></param>
        /// <param name="hero"></param>
        public void SetDistance(int distance, int hero) {
            switch (hero) {
                case 1:
                    this.Hero1Distance = distance;
                    return;

                case 2:
                    this.Hero2Distance = distance;
                    return;
            }
        }

        public bool ToggleOn {
            get {
                return toggleOn;
            }

            set {
                toggleOn = value;
            }
        }

        public int x {
            get {
                return this.position.x;
            }
        }

        public int y {
            get {
                return this.position.y;
            }
        }

        public IntVector2 Position {
            get {
                return position;
            }
        }

        /// <summary>
        /// Returns whether or not the cell has a mark
        /// </summary>
        /// <param name="hero"></param>
        /// <returns></returns>
        public bool HasMark(int hero) {
            switch (hero) {
                case 1:
                    return this.mark1 >= 0;

                case 2:
                    return this.mark2 >= 0;
            }

            Debug.LogError("Wrong hero: "+ hero);
            return false;
        }

        /// <summary>
        /// Returns the mark for the specified hero
        /// </summary>
        /// <param name="hero"></param>
        /// <returns></returns>
        public int GetMark(int hero) {
            switch (hero) {
                case 1:
                    return this.Mark1;

                case 2:
                    return this.Mark2;
            }

            Debug.LogError("Wrong hero: " + hero);
            return -1;
        }

    }
}
