using System;
using System.Collections.Generic;

using ShadowStar.Graph;

namespace ShadowStar.AStar {
	/// <summary>
	/// An object pool of AStarNode instances.
	/// </summary>
	class AStarNodePool<T> {
		
		private LinkedList<AStarNode<T>> inactiveList;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.AStar.AStarNodePool"/> class.
		/// </summary>
		public AStarNodePool() {
			inactiveList = new LinkedList<AStarNode<T>>();
		}
		
		/// <summary>
		/// Requests for an AStarNode instance.
		/// </summary>
		public AStarNode<T> Request(GraphNode<T, Link> graphNode) {
			AStarNode<T> instance = null;
			
			if(inactiveList.Count > 0) {
				// there are unused ones
				// we reuse them
				instance = inactiveList.Last.Value;
				instance.Init(graphNode);
				inactiveList.RemoveLast();
				return instance;
			}
			
			return new AStarNode<T>(graphNode);
		}
		
		/// <summary>
		/// Returns the specified instance to the pool.
		/// </summary>
		public void Return(AStarNode<T> node) {
			inactiveList.AddFirst(node);
		}
		
	}
}

