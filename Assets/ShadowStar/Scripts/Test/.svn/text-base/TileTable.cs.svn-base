using System;

namespace ShadowStar.Test {
	public class TileTable {
		
		private int width;
		private int height;
		
		private TestTile[] tiles;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.Test.TileTable"/> class.
		/// </summary>
		/// <param name='width'>
		/// Width.
		/// </param>
		/// <param name='height'>
		/// Height.
		/// </param>
		public TileTable(int width, int height) {
			this.width = width;
			this.height = height;
			tiles = new TestTile[width * height];
		}
		
		/// <summary>
		/// Add the specified tile to the table at position x, y. (0, 0) starts at top-left.
		/// </summary>
		/// <param name='tile'>
		/// Tile.
		/// </param>
		/// <param name='x'>
		/// X.
		/// </param>
		/// <param name='y'>
		/// Y.
		/// </param>
		public void Set(TestTile tile, int x, int y) {
			int index = ComputeLinearIndex(x, y);
			Assertion.Assert(tiles[index] == null, "Tile at the specified position should be empty.");
			tiles[index] = tile;
		}
		
		/// <summary>
		/// Returns the tile at the specified position.
		/// </summary>
		/// <param name='x'>
		/// X.
		/// </param>
		/// <param name='y'>
		/// Y.
		/// </param>
		public TestTile Get(int x, int y) {
			int index = ComputeLinearIndex(x, y);
			Assertion.AssertNotNull(tiles[index]);
			return tiles[index];
		}
		
		private int ComputeLinearIndex(int x, int y) {
			return y * this.width + x;
		}
	}
}

