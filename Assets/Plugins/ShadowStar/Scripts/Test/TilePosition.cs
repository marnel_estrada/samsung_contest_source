using System;
using UnityEngine;

namespace ShadowStar.Test {
	[Serializable]
	public class TilePosition {
		
		public int x;
		public int y;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.Test.TilePosition"/> class.
		/// </summary>
		public TilePosition() : this(0, 0) {
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.Test.TilePosition"/> struct.
		/// </summary>
		/// <param name='x'>
		/// X.
		/// </param>
		/// <param name='y'>
		/// Y.
		/// </param>
		public TilePosition(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
	}
}
