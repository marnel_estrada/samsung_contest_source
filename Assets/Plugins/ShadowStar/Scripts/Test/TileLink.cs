using System;

using ShadowStar.AStar;

namespace ShadowStar.Test {
	/// <summary>
	/// The cost of tile movement.
	/// </summary>
	public class TileLink : Link {
		
		private TestTile dest;
		private float distance;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.Test.TileLink"/> class.
		/// </summary>
		/// <param name='start'>
		/// Start.
		/// </param>
		/// <param name='dest'>
		/// Destination.
		/// </param>
		public TileLink(TestTile start, TestTile dest) {
			this.dest = dest;
			
			int hDisplacement = Math.Abs(dest.TilePosition.x - start.TilePosition.x);
			int vDisplacement = Math.Abs(dest.TilePosition.y - start.TilePosition.y);
			
			if(hDisplacement == 0 || vDisplacement == 0) {
				// this means that movement is adjacent
				distance = 10;
			} else {
				// this means that movement is diagonal
				distance = 14;
			}
		}
		
		#region Link implementation
		public float GetDistance() {
			return distance;
		}

		public bool IsReachable() {
			return dest.Reachable();
		}
		#endregion
	}
}
