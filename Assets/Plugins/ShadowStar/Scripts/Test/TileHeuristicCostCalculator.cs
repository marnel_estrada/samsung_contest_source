using System;

using ShadowStar.AStar;

namespace ShadowStar.Test {
	public class TileHeuristicCostCalculator : HeuristicCostCalculator<TestTile> {
		private TileHeuristicCostCalculator() {
		}

		#region HeuristicCostCalculator implementation
		public float ComputeCost(TestTile start, TestTile goal) {
			TilePosition startPosition = start.TilePosition;
			TilePosition goalPosition = goal.TilePosition;
			
			int xDistance = Math.Abs(startPosition.x - goalPosition.x);
			int yDistance = Math.Abs(startPosition.y - goalPosition.y);
			if(xDistance > yDistance) {
				return 10 * yDistance + (10 * (xDistance - yDistance));
			}
			
			return 10 * xDistance + (10 * (yDistance - xDistance));
		}
		#endregion
		
		private static readonly TileHeuristicCostCalculator ONLY_INSTANCE = new TileHeuristicCostCalculator();
		
		/// <summary>
		/// Returns the single instance of this class.
		/// </summary>
		public static TileHeuristicCostCalculator GetInstance() {
			return ONLY_INSTANCE;
		}
	}
}

