using System;

using ShadowStar.Utils;

/// <summary>
/// Comparator for integers. We implement as singleton since this class has no states.
/// </summary>
public class IntComparator : BinaryHeapComparator<int> {
	
	private IntComparator() {
	}

	public bool LesserThan(int a, int b) {
		return a < b;
	}
	
	public bool IsSameInstance(int a, int b) {
		return a == b;
	}
	
	private static IntComparator ONLY_INSTANCE = new IntComparator();
	
	/// <summary>
	/// Returns the only IntComparator instance.
	/// </summary>
	/// <returns>
	/// The single instance.
	/// </returns>
	public static IntComparator GetInstance() {
		return ONLY_INSTANCE;
	}
	
}

