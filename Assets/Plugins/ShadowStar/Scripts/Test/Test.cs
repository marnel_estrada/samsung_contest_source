using UnityEngine;
using System.Collections;

using ShadowStar.Utils;

public class Test : MonoBehaviour {

	// Use this for initialization
	void Start() {
	
		BinaryHeap<int> heap = new BinaryHeap<int>(IntComparator.GetInstance());
		
		int max = 31;
		for(int i = 0; i < max; ++i) {
			heap.Add(Random.Range(1, 101));
			//heap.Add(max - i);
			Debug.Log(heap.ToString());
		}
		
		while(!heap.IsEmpty()) {
			heap.RemoveRoot();
			Debug.Log(heap.ToString());
		}
	}
	
}

