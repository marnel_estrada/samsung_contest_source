using UnityEngine;
using System.Collections;

using ShadowStar.AStar;
using ShadowStar.Test;

public class TestTile : MonoBehaviour {
	
	[SerializeField]
	private bool reachable = true; // defaults to true
	
	private NodeHandle<TestTile> nodeHandle; // a-star node handle
	private TilePosition tilePosition;
	
	/// <summary>
	/// Initializes the tile.
	/// </summary>
	/// <param name='nodeHandle'>
	/// Node handle.
	/// </param>
	public void Init(NodeHandle<TestTile> nodeHandle, TilePosition tilePosition) {
		this.nodeHandle = nodeHandle;
		this.tilePosition = tilePosition;
	}
	
	/// <summary>
	/// Returns whether or not the tile is reachable.
	/// </summary>
	public bool Reachable() {
		return reachable;
	}
	
	/// <summary>
	/// Sets whether the tile is reachable or not.
	/// </summary>
	public void SetReachable(bool reachable) {
		this.reachable = reachable;
	}
	
	/// <summary>
	/// Returns the a-star handle.
	/// </summary>
	/// <value>
	/// The handle.
	/// </value>
	public NodeHandle<TestTile> Handle {
		get {
			return nodeHandle;
		}
	}
	
	/// <summary>
	/// Gets the tile position.
	/// </summary>
	public TilePosition TilePosition {
		get {
			return tilePosition;
		}
	}
}
