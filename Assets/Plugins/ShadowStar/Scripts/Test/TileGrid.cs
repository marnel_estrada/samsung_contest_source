using UnityEngine;
using System.Collections.Generic;

using ShadowStar.AStar;
using  ShadowStar.Test;

public class TileGrid : MonoBehaviour {
	
	[SerializeField]
	private GameObject tilePrefab;
	
	[SerializeField]
	private float tileSize;
	
	[SerializeField]
	private int gridWidth;
	
	[SerializeField]
	private int gridHeight;
	
	[SerializeField]
	private int startX;
	
	[SerializeField]
	private int startY;
	
	[SerializeField]
	private int targetX;
	
	[SerializeField]
	private int targetY;
	
	[SerializeField]
	private TilePosition[] unreachableTiles;
	
	private TileTable table;
	
	private AStar<TestTile> star;
	
	private NodeHandle<TestTile> startHandle;
	private NodeHandle<TestTile> targetHandle;
	
	private AStarPath<TestTile> path;

	// Use this for initialization
	void Start() {
		CreateTiles();
		PrepareAStar();
		
		path = new AStarPath<TestTile>();
	}
	
	void OnGUI() {
		GUILayout.BeginVertical();
		if(GUILayout.Button("Go")) {
			PerformAStar();
		}
		GUILayout.EndVertical();
	}
	
	private void PerformAStar() {
		// find handle of start and end
		startHandle = table.Get(startX, startY).Handle;
		targetHandle = table.Get(targetX, targetY).Handle;
		
		star.ResolvePath(path, startHandle, targetHandle, TileHeuristicCostCalculator.GetInstance());
		if(path.Reachable) {
			while(path.HasMorePath()) {
                TestTile pathTile = path.NextPath();
                SetColor(pathTile.gameObject, Color.yellow);
            }
		} else {
			Debug.Log("Goal unreachable");
		} 
	}
	
	private void CreateTiles() {
		table = new TileTable(gridWidth, gridHeight);
		
		// identify starting position of the top-left tile
		float worldHalfWidth = tileSize * gridWidth * 0.5f;
		float worldHalfHeight = tileSize * gridHeight * 0.5f;
		Vector3 bottomLeftPosition = new Vector3();
		bottomLeftPosition.x = -worldHalfWidth + (tileSize * 0.5f);
		bottomLeftPosition.y = -worldHalfHeight + (tileSize * 0.5f);
		
		// create each tile
		Vector3 tilePosition = new Vector3();
		Transform selfTransform = this.transform;
		for(int tileX = 0; tileX < gridWidth; ++tileX) {
			for(int tileY = 0; tileY < gridHeight; ++tileY) {
				tilePosition.x = bottomLeftPosition.x + (tileX * tileSize);
				tilePosition.y = bottomLeftPosition.y + (tileY * tileSize);
				GameObject tileObject = (GameObject) Instantiate(tilePrefab, tilePosition, Quaternion.identity);
				ColorTile(tileObject, tileX, tileY);
				tileObject.name = "Tile(" + tileX + ", " + tileY + ")"; 
				TestTile tile = tileObject.GetComponent<TestTile>();
				table.Set(tile, tileX, tileY);
				
				// set parent to this object so as not to clutter heirarchy
				tileObject.transform.parent = selfTransform;
			}
		}
	}
	
	private void ColorTile(GameObject tileObject, int x, int y) {
		if(x == startX && y == startY) {
			SetColor(tileObject, Color.green);
		}
		
		if(x == targetX && y == targetY) {
			SetColor(tileObject, Color.red);
		}
	}
	
	private void SetColor(GameObject tileObject, Color color) {
		Renderer[] renderers = tileObject.GetComponentsInChildren<Renderer>();
		foreach(Renderer renderer in renderers) {
			renderer.material.color = color;
		}
	}
	
	/// <summary>
	/// Prepares the neighboring paths of each tile.
	/// </summary>
	private void PrepareAStar() {
		star = new AStar<TestTile>();
		
		// add all tiles first
		for(int tileX = 0; tileX < gridWidth; ++tileX) {
			for(int tileY = 0; tileY < gridHeight; ++tileY) {
				Debug.Log("Preparing " + tileX + ", " + tileY);
				TestTile tile = table.Get(tileX, tileY);
				NodeHandle<TestTile> handle = star.Add(tile);
				tile.Init(handle, new TilePosition(tileX, tileY));
			}
		}
		
		// resolve neighbors
		for(int tileX = 0; tileX < gridWidth; ++tileX) {
			for(int tileY = 0; tileY < gridHeight; ++tileY) {
				PrepareNeighbors(tileX, tileY);
			}
		}
		
		// set unreachable tiles
		foreach(TilePosition position in unreachableTiles) {
			TestTile tile = table.Get(position.x, position.y);
			tile.SetReachable(false);
			SetColor(tile.gameObject, Color.blue);
		}
	}
	
	private void PrepareNeighbors(int x, int y) {
		// add path to neighbors
		TestTile tile = table.Get(x, y);
		TilePosition neighborPosition = new TilePosition();
		
		// top left
		neighborPosition.x = x - 1;
		neighborPosition.y = y + 1;
		//AddNeighbor(tile, neighborPosition);
		
		// top
		neighborPosition.x = x;
		neighborPosition.y = y + 1;
		AddNeighbor(tile, neighborPosition);
		
		// top right
		neighborPosition.x = x + 1;
		neighborPosition.y = y + 1;
		//AddNeighbor(tile, neighborPosition);
		
		// left
		neighborPosition.x = x - 1;
		neighborPosition.y = y;
		AddNeighbor(tile, neighborPosition);
		
		// right
		neighborPosition.x = x + 1;
		neighborPosition.y = y;
		AddNeighbor(tile, neighborPosition);
		
		// bottom left
		neighborPosition.x = x - 1;
		neighborPosition.y = y - 1;
		//AddNeighbor(tile, neighborPosition);
		
		// bottom
		neighborPosition.x = x;
		neighborPosition.y = y - 1;
		AddNeighbor(tile, neighborPosition);
		
		// bottom right
		neighborPosition.x = x + 1;
		neighborPosition.y = y - 1;
		//AddNeighbor(tile, neighborPosition);
	}
	
	private void AddNeighbor(TestTile tile, TilePosition neighborPosition) {
		if(neighborPosition.x < 0 || neighborPosition.x >= gridWidth) {
			// out of the map
			return;
		}
		
		if(neighborPosition.y < 0 || neighborPosition.y >= gridHeight) {
			// out of the map
			return;
		}
		
		TestTile neighbor = table.Get(neighborPosition.x, neighborPosition.y);
		tile.Handle.AddPath(neighbor.Handle, new TileLink(tile, neighbor));
	}
	
}
