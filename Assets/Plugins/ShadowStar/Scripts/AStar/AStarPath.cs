using System;
using System.Collections.Generic;

using Common.Utils;

namespace ShadowStar.AStar {
	/// <summary>
	/// Handles and manages resolution of a path.
	/// </summary>
	public class AStarPath<T> {
		
		private SimpleList<T> sequence;
		private bool reachable;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.AStar.AStarPath"/> class.
		/// </summary>
		public AStarPath() {
			sequence = new SimpleList<T>();
			reachable = false;
		}
		
		/// <summary>
		/// Adds a position to the path.
		/// Note that positions added here are added in reverse order. This is to save another pass 
		/// on the list of path when we have to order them like in the previous implementation.
		/// </summary>
		public void Add(T position) {
			sequence.Add(position);
		}
		
		/// <summary>
		/// Clear the path.
		/// </summary>
		public void Clear() {
			sequence.Clear();
			reachable = false;
		}
		
		/// <summary>
		/// Marks as reachable.
		/// </summary>
		public void MarkAsReachable() {
			reachable = true;
		}
		
		/// <summary>
		/// Unmarks as reachable.
		/// </summary>
		public void UnmarkAsReachable() {
			reachable = false;
		}
		
		/// <summary>
		/// Gets a value indicating whether this <see cref="ShadowStar.AStar.AStarPath"/> is reachable.
		/// </summary>
		public bool Reachable {
			get {
				return reachable;
			}
		}

		private int currentIndex;

		/**
		 * Moves the cursor back to the starting point
		 */
		public void RestartPath() {
			// note that we are traversing in reverse order since the positions added here were destination first to starting position
			this.currentIndex = this.sequence.Count;
		}

		/**
		 * Returns whether or not there are more path
		 */
		public bool HasMorePath() {
            if(this.sequence.Count == 0) {
                return false;
            }

			return this.currentIndex - 1 >= 0;
		}

		/**
		 * Returns the current path and moves to next path
		 */
		public T NextPath() {
			--this.currentIndex;
			return this.sequence[this.currentIndex];
		}

		/**
		 * The last in the path is the destination
		 */
		public T GetDestination() {
			// note that positions are added AStar in reverse order
			return this.sequence[0];
		}

		/**
		 * Returns the number of items in the path
		 */
		public int PathCount {
			get {
				return this.sequence.Count;
			}
		}
		
	}
}
