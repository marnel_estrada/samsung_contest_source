using System;

namespace ShadowStar.AStar {
	/// <summary>
	/// Interface for the heuristic cost calculator used in AStar.
	/// </summary>
	public interface HeuristicCostCalculator<T> {
		
		/// <summary>
		/// Computes the heuristic cost from the specified starting position and the goal.
		/// </summary>
		/// <returns>
		/// The cost.
		/// </returns>
		/// <param name='start'>
		/// Node.
		/// </param>
		/// <param name='goal'>
		/// Goal.
		/// </param>
		float ComputeCost(T start, T goal);
		
	}
	
}

