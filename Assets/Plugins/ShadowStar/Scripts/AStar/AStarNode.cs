using System;

using ShadowStar.Graph;

namespace ShadowStar.AStar {
	/// <summary>
	/// Holds the search data of each node during search.
	/// </summary>
	public class AStarNode<T> {
		
		private GraphNode<T, Link> graphNode;
		
		private AStarNode<T> parent;
		
		private float gScore; // the G in the AStar algorithm
		
		private float hScore; // the H in the AStar algorithm
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.AStar.AStarNode"/> class.
		/// </summary>
		/// <param name='graphNode'>
		/// Handle.
		/// </param>
		public AStarNode(GraphNode<T, Link> graphNode) {
			this.graphNode = graphNode;
			Reset();
		}
		
		/// <summary>
		/// Initializes the AStar node with a specified graph node. We provide this function so that we can implement a pool of AStarNode instances instead of recreating them per AStar query.
		/// </summary>
		/// <param name='graphNode'>
		/// Graph node.
		/// </param>
		public void Init(GraphNode<T, Link> graphNode) {
			this.graphNode = graphNode;
			Reset();
		}
		
		/**
		 * Resets the states of the search path.
		 */
		public void Reset() {
			parent = null;
			gScore = 0;
			hScore = 0;
		}
		
		/// <summary>
		/// Gets or sets the G.
		/// </summary>
		/// <value>
		/// The G.
		/// </value>
		public float G {
			get {
				return gScore;
			}
			
			set {
				gScore = value;
			}
		}
		
		/// <summary>
		/// Gets or sets the H.
		/// </summary>
		/// <value>
		/// The H.
		/// </value>
		public float H {
			get {
				return hScore;
			}
			
			set {
				hScore = value;
			}
		}
		
		/// <summary>
		/// Gets the F.
		/// </summary>
		/// <value>
		/// The F.
		/// </value>
		public float F {
			get {
				return gScore + hScore;
			}
		}
		
		/// <summary>
		/// Gets or sets the parent.
		/// </summary>
		/// <value>
		/// The parent.
		/// </value>
		public AStarNode<T> Parent {
			get {
				return parent;
			}
			
			set {
				parent = value;
			}
		}
		
		/// <summary>
		/// Returns whether or not the node is that starting point.
		/// </summary>
		public bool IsStartNode() {
			return parent == null;
		}
		
		/// <summary>
		/// Returns the handle stored in this node.
		/// </summary>
		/// <value>
		/// The handle.
		/// </value>
		public GraphNode<T, Link> GraphNode {
			get {
				return graphNode;
			}
		}
		
		/// <summary>
		/// Returns the T stored in this node.
		/// </summary>
		public T Position {
			get {
				return graphNode.Value;
			}
		}
		
	}
}
