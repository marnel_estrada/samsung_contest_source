using System;
using System.Collections.Generic;

using Common.Utils;

using ShadowStar.Graph;
using ShadowStar.AStar;
using ShadowStar.Utils;

namespace ShadowStar.AStar {
	public class AStar<T> {
		
		private Graph<T, Link> graph;
		
		private OpenSet<T> openSet;
		private Dictionary<GraphNode<T, Link>, AStarNode<T>> closeSet;
		private AStarNodePool<T> nodePool;

		private Dictionary<T, NodeHandle<T>> handleMap;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.AStar.AStar`2"/> class.
		/// </summary>
		public AStar() {
			this.graph = new Graph<T, Link>();
			this.openSet = new OpenSet<T>();
			this.closeSet = new Dictionary<GraphNode<T, Link>, AStarNode<T>>();
			this.nodePool = new AStarNodePool<T>();
			this.handleMap = new Dictionary<T, NodeHandle<T>>();
		}
		
		/// <summary>
		/// Adds a node to the AStar map. 
		/// Client code may add neighbors and cost to the new node. 
		/// Client may also maintain a separate manager for easier NodeHandle resolution.
		/// </summary>
		/// <param name='node'>
		/// Node.
		/// </param>
		public NodeHandle<T> Add(T position) {
			// check if already existing
			NodeHandle<T> handle = null;
			if(this.handleMap.TryGetValue(position, out handle)) {
				// already existing
				return handle;
			}

			// create a new one and cache
			handle = new NodeHandle<T>(graph.Add(position));
			this.handleMap[position] = handle;

			return handle;
		}

		/// <summary>
		/// Returns the handle for the specified position
		/// </summary>
		/// <param name="position">Position.</param>
		public NodeHandle<T> Get(T position) {
			NodeHandle<T> handle = null;
			Assertion.Assert(this.handleMap.TryGetValue(position, out handle)); // should be existing

			return handle;
		}
		
		/// <summary>
		/// This is used for resolving path using properties of the position (tile) instead of the actual position reference.
		/// For example, start tile is in a body of water which is reachable and you want to look for the closest land. 
		/// This is also handy if you don't know what the goal position is. You just know its properties.
		/// </summary>
		/// <param name='path'>
		/// Path.
		/// </param>
		/// <param name='start'>
		/// Start.
		/// </param>
		/// <param name='goalChecker'>
		/// The goal checker.
		/// </param>
		/// <param name='calculator'>
		/// Calculator.
		/// </param>
		/// <param name='reachability'>
		/// Rechability.
		/// </param>
		public void ResolvePath(AStarPath<T> path, NodeHandle<T> start, GoalIdentifier<T> goalIdentifier, HeuristicCostCalculator<T> calculator, Reachability<T> reachability = null) {
			path.Clear();
			
			ClearOpenSet();
			closeSet.Clear();
			
			// provide an instance manager to AStarNode objects
			AStarNode<T> startNode = nodePool.Request(start.RawNode);
			startNode.G = 0;
			startNode.H = 0; // H is zero because this function does not have a specific target goal
			openSet.Add(startNode);
			
			while(!openSet.IsEmpty()) {
				AStarNode<T> current = openSet.GetNodeWithLeastF(); // returns the AStar node with the lowest F value
				if(goalIdentifier.IsGoal(current.Position)) {
					ConstructPath(current, path);
					path.MarkAsReachable();
					return;
				}
				
				openSet.RemoveNodeWithLeastF();
				closeSet.Add(current.GraphNode, current);
				
				// note that we pass in a null goal node here
				// this will be checked by ProcessNode() and assigns zero to the H value of neighbors
				ProcessNode(current, null, calculator, reachability);
			}
			
			// unreachable
			path.UnmarkAsReachable();
		}
		
		/// <summary>
		/// Resolves the shortest path from start to destination. Sequence of positions are stored in the specified path.
		/// </summary>
		/// <param name='start'>
		/// Start.
		/// </param>
		/// <param name='goal'>
		/// Destination.
		/// </param>
		/// <param name='path'>
		/// Path.
		/// </param>
		public void ResolvePath(AStarPath<T> path, NodeHandle<T> start, NodeHandle<T> goal, HeuristicCostCalculator<T> calculator, Reachability<T> reachability = null) {
			path.Clear();

            if(reachability != null && !reachability.IsReachable(goal.Position)) {
                // No need to continue AStar if the position is unreachable just by itself
                path.UnmarkAsReachable();
                return;
            }
			
			ClearOpenSet();
			closeSet.Clear();
			
			// provide an instance manager to AStarNode objects
			AStarNode<T> startNode = nodePool.Request(start.RawNode);
			startNode.G = 0;
			startNode.H = calculator.ComputeCost(start.Position, goal.Position);
			openSet.Add(startNode);
			
			while(!openSet.IsEmpty()) {
				AStarNode<T> current = openSet.GetNodeWithLeastF(); // returns the AStar node with the lowest F value
				if(current.GraphNode == goal.RawNode) {
					ConstructPath(current, path);
					path.MarkAsReachable();
					return;
				}
				
				openSet.RemoveNodeWithLeastF();
				closeSet.Add(current.GraphNode, current);
				
				ProcessNode(current, goal, calculator, reachability);
			}
			
			// unreachable
			path.UnmarkAsReachable();
		}
		
		private void ClearOpenSet() {
			// we clear manually so that we reuse node instances
			while(!openSet.IsEmpty()) {
				AStarNode<T> removed = openSet.RemoveNodeWithLeastF();
				nodePool.Return(removed);
			}
		}
		
		/// <summary>
		/// Processes an A star node during path recognition.
		/// </summary>
		private void ProcessNode(AStarNode<T> current, NodeHandle<T> goal, HeuristicCostCalculator<T> calculator, Reachability<T> reachability) {
			int arcCount = current.GraphNode.ArcCount;
			for(int i = 0; i < arcCount; ++i) {
				GraphArc<T, Link> arc = current.GraphNode.GetArcAt(i);
				GraphNode<T, Link> neighbor = arc.Destination;
				
				// note that Weight is the Link instance
				if(!arc.Weight.IsReachable()) {
					// not reachable by link
					continue;
				}
				
				if(closeSet.ContainsKey(neighbor)) {
					// already in closed set
					continue;
				}

				if(reachability != null && !reachability.IsReachable(current.GraphNode.Value, neighbor.Value)) {
					// not reachable by game specific rules
					continue;
				}
				
				// note that weight is the distance between the node and the neighbor
				float tentativeG = current.G + arc.Weight.GetDistance();
				
				if(openSet.Contains(neighbor)) {
					// If an adjacent square is already on the open list, check to see if this path to that square is a better one.
					// In other words, check to see if the G score for that square is lower if we use the current square to get there. If not, don’t do anything.
					// On the other hand, if the G cost of the new path is lower, change the parent of the adjacent square to the selected square
					AStarNode<T> neighborNode = openSet.Get(neighbor);
					if(tentativeG < neighborNode.G) {
						// this is a better path, we change parent using the current node and assign the new lower G
						neighborNode.Parent = current;
						neighborNode.G = tentativeG;
						openSet.Fix(neighborNode); // note here that we are fixing the ordering since the G value has changed
					}
				} else {
					// add squares to the open list if they are not on the open list already. Make the selected square the “parent” of the new squares.
					AStarNode<T> neighborNode = nodePool.Request(neighbor);
					neighborNode.Parent = current;
					neighborNode.G = tentativeG;
					
					if(goal == null) {
						// client code can pass in a null goal node which makes the algorithm into Djiktra's traversal
						// this is used for cases when client code is looking for certain properties of the position instead of an actual position
						neighborNode.H = 0;
					} else {
						neighborNode.H = calculator.ComputeCost(neighbor.Value, goal.Position);
					}
					
					openSet.Add(neighborNode);
				}
			}
		}
		
		private void ConstructPath(AStarNode<T> node, AStarPath<T> path) {
			// note here that we no longer need to reverse the ordering of the path
			// we just add them as reversed in AStarPath
			// AStarPath then knows how to handle this
			AStarNode<T> current = node;
			while(!current.IsStartNode()) {
				path.Add(current.Position);
				current = current.Parent;
			}
		}
		
	}
}
