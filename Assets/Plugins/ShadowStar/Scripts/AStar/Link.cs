using System;

namespace ShadowStar.AStar {
	public interface Link {
		
		/// <summary>
		/// Returns the distance of the link.
		/// </summary>
		float GetDistance();
		
		/// <summary>
		/// Returns whether or not the link is reachable.
		/// </summary>
		bool IsReachable();
		
	}
}
