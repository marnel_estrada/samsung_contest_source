using System;

using ShadowStar.Utils;

namespace ShadowStar.AStar {
	/// <summary>
	/// Binary heap comparator for AStarNode instances.
	/// </summary>
	class AStarNodeComparator<T> : BinaryHeapComparator<AStarNode<T>> {
		
		private AStarNodeComparator() {
		}
		
		#region BinaryHeapComparator[AStarNode] implementation
		public bool LesserThan(AStarNode<T> a, AStarNode<T> b) {
			return a.F < b.F;
		}
		
		public bool IsSameInstance(AStarNode<T> a, AStarNode<T> b) {
			return a == b;
		}
		#endregion
		
		private static readonly AStarNodeComparator<T> ONLY_INSTANCE = new AStarNodeComparator<T>();
		
		/// <summary>
		/// Returns the only AStarNodeComparator instance.
		/// </summary>
		/// <returns>
		/// The instance.
		/// </returns>
		public static AStarNodeComparator<T> GetInstance() {
			return ONLY_INSTANCE;
		}
		
	}
}

