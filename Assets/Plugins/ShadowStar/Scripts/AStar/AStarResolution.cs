﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common;

using ShadowStar.Graph;
using ShadowStar.Utils;

namespace ShadowStar.AStar {
    /// <summary>
    /// An algorithm class that performs an A* search
    /// Usage:
    /// AStarResolution<Tile> resolution = new AStarResolution<Tile>();
    /// resolution.Init(...);
    /// resolution.Execute();
    /// use resolution.Done to check if the execution is done
    /// </summary>
    public class AStarResolution<T> {

        private OpenSet<T> openSet = new OpenSet<T>();
        private Dictionary<GraphNode<T, Link>, AStarNode<T>> closeSet = new Dictionary<GraphNode<T, Link>, AStarNode<T>>();
        private AStarNodePool<T> nodePool = new AStarNodePool<T>();

        private readonly AStarPath<T> internalPath = new AStarPath<T>();

        private AStarPath<T> executionPath; // Path used in execution (may use external path)

        private NodeHandle<T> start;
        private NodeHandle<T> goal;
        private HeuristicCostCalculator<T> calculator;
        private Reachability<T> reachability;

        private GoalIdentifier<T> goalIdentifier;

        // Default to done so it can be check that the client code can request for AStar since the last request is already done
        private bool done = true;
        private bool cancelled = false;

        /// <summary>
        /// We use an empty constructor so we could use this class in a pool
        /// </summary>
        public AStarResolution() {
        }

        /// <summary>
        /// Initializer
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goal"></param>
        /// <param name="calculator"></param>
        /// <param name="reachability"></param>
        public void Init(NodeHandle<T> start, NodeHandle<T> goal, HeuristicCostCalculator<T> calculator, Reachability<T> reachability = null) {
            this.start = start;
            this.goal = goal;
            this.goalIdentifier = null;
            this.calculator = calculator;
            this.reachability = reachability;

            this.done = false;
            this.cancelled = false;
        }

        /// <summary>
        /// Initializer for AStar search that uses a GoalIdentifier instead of a position goal
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goalIdentifier"></param>
        /// <param name="calculator"></param>
        /// <param name="reachability"></param>
        public void Init(NodeHandle<T> start, GoalIdentifier<T> goalIdentifier, HeuristicCostCalculator<T> calculator, Reachability<T> reachability = null) {
            this.start = start;
            this.goal = null;
            this.goalIdentifier = goalIdentifier;
            this.calculator = calculator;
            this.reachability = reachability;

            this.done = false;
            this.cancelled = false;
        }

        private void ClearOpenSet() {
            // we clear manually so that we reuse node instances
            while (!openSet.IsEmpty()) {
                AStarNode<T> removed = openSet.RemoveNodeWithLeastF();
                nodePool.Return(removed);
            }
        }

        /// <summary>
        /// Executes the search
        /// </summary>
        public virtual void Execute() {
            Execute(this.internalPath);
        }

        /// <summary>
        /// Executes the search using an external path
        /// </summary>
        /// <param name="externalPath"></param>
        public void Execute(AStarPath<T> externalPath) {
            this.executionPath = externalPath;

            try {
                Search();
            } finally {
                // Set done to true if there are exceptions that happened
                this.done = true;
            }
        }

        public bool IsDone {
            get {
                return done;
            }
        }

        public bool IsCancelled {
            get {
                return cancelled;
            }
        }

        public AStarPath<T> Path {
            get {
                return internalPath;
            }
        }

        protected NodeHandle<T> Start {
            get {
                return start;
            }
        }

        protected NodeHandle<T> Goal {
            get {
                return goal;
            }
        }

        protected OpenSet<T> OpenSet {
            get {
                return openSet;
            }
        }

        protected Dictionary<GraphNode<T, Link>, AStarNode<T>> CloseSet {
            get {
                return closeSet;
            }
        }

        protected AStarNodePool<T> NodePool {
            get {
                return nodePool;
            }
        }

        protected HeuristicCostCalculator<T> Calculator {
            get {
                return calculator;
            }
        }

        protected Reachability<T> Reachability {
            get {
                return reachability;
            }
        }

        public GoalIdentifier<T> GoalIdentifier {
            get {
                return goalIdentifier;
            }
        }

        /// <summary>
        /// Marks the resolution as cancelled so that when it's checked for execution, it will not continue
        /// </summary>
        public void MarkAsCancelled() {
            this.cancelled = true;
        }

        /// <summary>
        /// Performs the A* search
        /// </summary>
        protected virtual void Search() {
            this.executionPath.Clear();

            if (reachability != null && this.goal != null && !reachability.IsReachable(goal.Position)) {
                // No need to continue AStar if the position is unreachable just by itself
                this.executionPath.UnmarkAsReachable();
                return;
            }

            ClearOpenSet();
            this.closeSet.Clear();

            // provide an instance manager to AStarNode objects
            AStarNode<T> startNode = nodePool.Request(start.RawNode);
            startNode.G = 0;
            startNode.H = 0;

            if (this.goal != null) {
                startNode.H = calculator.ComputeCost(start.Position, this.goal.Position);
            }

            openSet.Add(startNode);

            while (!openSet.IsEmpty()) {
                AStarNode<T> current = openSet.GetNodeWithLeastF(); // returns the AStar node with the lowest F value
                if (IsGoal(current)) {
                    ConstructPath(current, this.executionPath);
                    this.executionPath.MarkAsReachable();
                    return;
                }

                openSet.RemoveNodeWithLeastF();
                ProcessNode(current, this.goal, calculator, reachability);

                if (!this.closeSet.ContainsKey(current.GraphNode)) {
                    closeSet.Add(current.GraphNode, current);
                }
            }

            // unreachable
            this.executionPath.UnmarkAsReachable();
        }

        /// <summary>
        /// Unmarks the path as reachable
        /// </summary>
        protected void UnmarkAsReachable() {
            this.executionPath.UnmarkAsReachable();
        }

        protected bool IsGoal(AStarNode<T> node) {
            // Position goal has more precedence
            if(this.goal != null) {
                return node.GraphNode == this.goal.RawNode;
            }

            // At this point, we are using a GoalIdentifier
            Assertion.AssertNotNull(this.goalIdentifier);
            return this.goalIdentifier.IsGoal(node.Position);
        }

        /// <summary>
        /// Processes an A star node during path recognition.
        /// </summary>
        protected virtual void ProcessNode(AStarNode<T> current, NodeHandle<T> goal, HeuristicCostCalculator<T> calculator, Reachability<T> reachability) {
            int arcCount = current.GraphNode.ArcCount;
            for (int i = 0; i < arcCount; ++i) {
                GraphArc<T, Link> arc = current.GraphNode.GetArcAt(i);
                GraphNode<T, Link> neighbor = arc.Destination;

                // note that Weight is the Link instance
                if (!arc.Weight.IsReachable()) {
                    // not reachable by link
                    continue;
                }

                if (closeSet.ContainsKey(neighbor)) {
                    // already in closed set
                    continue;
                }

                if (reachability != null && !reachability.IsReachable(current.GraphNode.Value, neighbor.Value)) {
                    // not reachable by game specific rules
                    continue;
                }

                // note that weight is the distance between the node and the neighbor
                float tentativeG = current.G + arc.Weight.GetDistance();

                if (openSet.Contains(neighbor)) {
                    // If an adjacent square is already on the open list, check to see if this path to that square is a better one.
                    // In other words, check to see if the G score for that square is lower if we use the current square to get there. If not, don’t do anything.
                    // On the other hand, if the G cost of the new path is lower, change the parent of the adjacent square to the selected square
                    AStarNode<T> neighborNode = openSet.Get(neighbor);
                    if (tentativeG < neighborNode.G) {
                        // this is a better path, we change parent using the current node and assign the new lower G
                        neighborNode.Parent = current;
                        neighborNode.G = tentativeG;
                        openSet.Fix(neighborNode); // note here that we are fixing the ordering since the G value has changed
                    }
                } else {
                    // add squares to the open list if they are not on the open list already. Make the selected square the “parent” of the new squares.
                    AStarNode<T> neighborNode = nodePool.Request(neighbor);
                    neighborNode.Parent = current;
                    neighborNode.G = tentativeG;

                    if (goal == null) {
                        // client code can pass in a null goal node which makes the algorithm into Djiktra's traversal
                        // this is used for cases when client code is looking for certain properties of the position instead of an actual position
                        neighborNode.H = 0;
                    } else {
                        neighborNode.H = calculator.ComputeCost(neighbor.Value, goal.Position);
                    }

                    openSet.Add(neighborNode);
                }
            }
        }

        protected virtual void ConstructPath(AStarNode<T> node, AStarPath<T> path) {
            // note here that we no longer need to reverse the ordering of the path
            // we just add them as reversed in AStarPath
            // AStarPath then knows how to handle this
            AStarNode<T> current = node;
            while (!current.IsStartNode()) {
                path.Add(current.Position);
                current = current.Parent;
            }
        }

    }
}
