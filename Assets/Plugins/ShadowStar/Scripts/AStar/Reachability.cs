using System;

namespace ShadowStar.AStar {
	/**
	 * An interface that is used by AStar to determine if a specific node is reachable. This is different from link specific reachability.
	 * This may be implemented by client code for game specific purposes which may not be determined from link information.
	 * For example, the reachability depends on that state of a specific game object.
	 */
	public interface Reachability<T> {

        /// <summary>
        /// A reachability check on a single position
        /// This is used to check if a goal is reachable at all
        /// If not, the search ends abruptly
        /// This is to avoid useless search when the position can't be reached at all
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        bool IsReachable(T position);

		/**
		 * Returns whether or not the specified position is reachable.
		 */
		bool IsReachable(T from, T to);
		
	}
}
