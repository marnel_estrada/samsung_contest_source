﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common;
using UnityThreading;
using Common.Utils;
using System.Threading;

namespace ShadowStar.AStar {
    /// <summary>
    /// A component that handles the single thread that processes AStar requests
    /// To use, create a subclass that specifies T
    /// </summary>
    public class AStarThreadQueue<T> {

        private bool running;
        private ActionThread thread;

        private Queue<AStarResolution<T>> queue = new Queue<AStarResolution<T>>();

        private Pool<AStarResolution<T>> resolutionPool = new Pool<AStarResolution<T>>();

        /// <summary>
        /// Runs the thread
        /// </summary>
        public void RunThread() {
            this.running = true;
            this.thread = UnityThreadHelper.CreateThread((Action)Process);
        }

        /// <summary>
        /// Stops the thread
        /// </summary>
        public void StopThread() {
            this.running = false;
            this.thread.Abort();
        }

        public bool IsAlive {
            get {
                return this.thread.IsAlive;
            }
        }

        /// <summary>
        /// Enqueues an AStar request using start and end positions
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goal"></param>
        /// <param name="calculator"></param>
        /// <param name="reachability"></param>
        /// <returns></returns>
        public AStarResolution<T> Enqueue(NodeHandle<T> start, NodeHandle<T> goal, HeuristicCostCalculator<T> calculator, Reachability<T> reachability = null) {
            lock (SYNC_LOCK) {
                AStarResolution<T> newResolution = RequestForDestination();
                newResolution.Init(start, goal, calculator, reachability);
                this.queue.Enqueue(newResolution);
                return newResolution;
            }
        }

        /// <summary>
        /// Enqueues an AStar request using start and a GoalIdentifier
        /// </summary>
        /// <param name="start"></param>
        /// <param name="goalIdentifier"></param>
        /// <param name="calculator"></param>
        /// <param name="reachability"></param>
        /// <returns></returns>
        public AStarResolution<T> Enqueue(NodeHandle<T> start, GoalIdentifier<T> goalIdentifier, HeuristicCostCalculator<T> calculator, Reachability<T> reachability = null) {
            lock (SYNC_LOCK) {
                AStarResolution<T> newResolution = RequestForGoalIdentifier();
                newResolution.Init(start, goalIdentifier, calculator, reachability);
                this.queue.Enqueue(newResolution);
                return newResolution;
            }
        }

        // We did it this way so it can be overridden
        // We separate resolution for destination from goal identifier because some A* algorithms work only with destination
        // For example, jump point search only works with destination
        protected virtual AStarResolution<T> RequestForDestination() {
            return this.resolutionPool.Request();
        }

        protected virtual AStarResolution<T> RequestForGoalIdentifier() {
            return this.resolutionPool.Request();
        }

        private static readonly object SYNC_LOCK = new object();

        private void Process() {
            while (this.running) {
                try {
                    AStarResolution<T> resolution = null;
                    lock(SYNC_LOCK) {
                        // Include queue.Count in the locking here based from https://www.reddit.com/r/gamedev/comments/6z7drj/nightmare_on_release_day/
                        if (this.queue.Count > 0) {
                            resolution = this.queue.Dequeue();
                        }
                    }

                    if(resolution == null) {
                        // No resolution to process
                        Thread.Sleep(1);
                        continue;
                    }

                    if (resolution.IsCancelled) {
                        // Already cancelled
                        // We recycle it ourselves
                        // Note here that client does not need to recycle requests marked as handled
                        Recycle(resolution);
                    } else {
                        // Run only if it's not cancelled yet
                        // The resolutions in the queue may be cancelled while they're still queued
                        try {
                            resolution.Execute();
                        } catch (Exception e) {
                            // Ignore errors so that the thread will not stop
                            // We log it anyway
                            resolution.MarkAsCancelled();
                            Debug.LogError("AStarThreadQueue.Process(): " + e.Message);
                        }
                    }
                } catch (Exception e) {
                    // Ignore errors so that the thread will not stop
                    // We log it anyway
                    Debug.LogError("AStarThreadQueue.Process(): " + e.Message);
                    Debug.LogError(e.StackTrace);
                }

                Thread.Sleep(1);
            }
        }

        /// <summary>
        /// Recycles the specified resolution
        /// </summary>
        /// <param name="resolution"></param>
        public virtual void Recycle(AStarResolution<T> resolution) {
            lock (SYNC_LOCK) {
                this.resolutionPool.Recycle(resolution);
            }
        }

        /// <summary>
        /// Returns the number of requests in the queue
        /// </summary>
        public int RequestCount {
            get {
                return this.queue.Count;
            }
        }

        public int PooledCount {
            get {
                return this.resolutionPool.Count;
            }
        }

    }
}
