using System;

using ShadowStar.Graph;

namespace ShadowStar.AStar {
	/// <summary>
	/// Wrapper for GraphNode in AStar such that client code don't have to deal with GraphNode instances.
	/// </summary>
	public class NodeHandle<T> {
		
		private GraphNode<T, Link> rawNode;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.AStar.NodeHandle"/> class.
		/// </summary>
		/// <param name='rawNode'>
		/// Raw node.
		/// </param>
		public NodeHandle(GraphNode<T, Link> rawNode) {
			this.rawNode = rawNode;
		}
		
		/// <summary>
		/// Gets the raw node.
		/// </summary>
		/// <value>
		/// The raw node.
		/// </value>
		public GraphNode<T, Link> RawNode {
			get {
				return rawNode;
			}
		}
		
		/// <summary>
		/// Adds a path to this node.
		/// </summary>
		/// <param name='destination'>
		/// Destination.
		/// </param>
		/// <param name='link'>
		/// Cost.
		/// </param>
		public void AddPath(NodeHandle<T> destination, Link link) {
			Assertion.AssertNotNull(destination);
			rawNode.AddArc(destination.RawNode, link);
		}
		
		/// <summary>
		/// Returns the Position model stored in this handle.
		/// </summary>
		/// <value>
		/// A star node.
		/// </value>
		public T Position {
			get {
				return rawNode.Value;
			}
		}
	}
}
