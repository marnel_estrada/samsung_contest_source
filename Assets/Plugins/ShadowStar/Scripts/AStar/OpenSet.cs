using System;
using System.Collections.Generic;

using ShadowStar.Utils;
using ShadowStar.Graph;

namespace ShadowStar.AStar {
	/// <summary>
	/// Used in AStar to manage the open set. Nodes are kept in heap for faster resolution of least F and in a dictionary for faster checking if the node is already in the open set.
	/// </summary>
	public class OpenSet<T> {
		
		private BinaryHeap<AStarNode<T>> heap;
		private Dictionary<GraphNode<T, Link>, AStarNode<T>> map;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.AStar.OpenSet"/> class.
		/// </summary>
		public OpenSet() {
			heap = new BinaryHeap<AStarNode<T>>(AStarNodeComparator<T>.GetInstance());
			map = new Dictionary<GraphNode<T, Link>, AStarNode<T>>();
		}
		
		/// <summary>
		/// Adds the specified node to the set.
		/// </summary>
		public void Add(AStarNode<T> node) {
			heap.Add(node);
			map.Add(node.GraphNode, node);
		}
		
		/// <summary>
		/// Clears the open set.
		/// </summary>
		public void Clear() {
			heap.Clear();
			map.Clear();
		}
		
		/// <summary>
		/// Returns whether or not the open set is empty.
		/// </summary>
		public bool IsEmpty() {
			return heap.IsEmpty();
		}
		
		/// <summary>
		/// Gets the node with least f.
		/// </summary>
		/// <returns>
		/// The node with least f.
		/// </returns>
		public AStarNode<T> GetNodeWithLeastF() {
			return heap.RootValue;
		}
		
		/// <summary>
		/// Removes the node with least f.
		/// </summary>
		public AStarNode<T> RemoveNodeWithLeastF() {
			// remove in heap and in map as well
			AStarNode<T> removedNode = heap.RemoveRoot();
			map.Remove(removedNode.GraphNode);
			
			return removedNode;
		}
		
		/// <summary>
		/// Returns whether or not the set already contains the specified node.
		/// </summary>
		public bool Contains(GraphNode<T, Link> node) {
			return map.ContainsKey(node);
		}
		
		/// <summary>
		/// Resolves the AStarNode with the specified graph node.
		/// </summary>
		public AStarNode<T> Get(GraphNode<T, Link> graphNode) {
			return map[graphNode];
		}
		
		/// <summary>
		/// Fixes the ordering of the heap from the specified node. It's G value may been updated so we must restructure the heap.
		/// </summary>
		public void Fix(AStarNode<T> node) {
			heap.Fix(node);
		}
		
	}
}

