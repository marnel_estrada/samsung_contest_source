using System;
using System.Collections.Generic;

using Common.Utils;

namespace ShadowStar.Graph {
	
	/// <summary>
	/// A Graph data structure.
	/// </summary>
	public class Graph<NodeType, ArcType> {
		
		private SimpleList<GraphNode<NodeType, ArcType>> nodeList;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.Graph.Graph`2"/> class.
		/// </summary>
		public Graph() {
			nodeList = new SimpleList<GraphNode<NodeType, ArcType>>();
		}
		
		/// <summary>
		/// Adds a graph node for the specified value. Returns the node associated with the value so that client code could manipulate it like adding arcs.
		/// </summary>
		/// <param name='nodeValue'>
		/// The node associated with the value
		/// </param>
		public GraphNode<NodeType, ArcType> Add(NodeType nodeValue) {
			GraphNode<NodeType, ArcType> node = new GraphNode<NodeType, ArcType>(nodeValue);
			nodeList.Add(node);
			return node;
		}
		
	}
	
}
