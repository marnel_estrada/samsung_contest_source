using System;

namespace ShadowStar.Graph {
	
	/// <summary>
	/// Represents an arc in a graph data structure.
	/// </summary>
	public class GraphArc<NodeType, ArcType> {
		
		private GraphNode<NodeType, ArcType> destinationNode;
		private ArcType weight;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="AssemblyCSharp.GraphArc`2"/> class.
		/// </summary>
		/// <param name='weight'>
		/// Weight.
		/// </param>
		public GraphArc(GraphNode<NodeType, ArcType> destinationNode, ArcType weight) {
			this.destinationNode = destinationNode;
			this.weight = weight;
		}
		
		/// <summary>
		/// Returns the destination node.
		/// </summary>
		/// <value>
		/// The destination.
		/// </value>
		public GraphNode<NodeType, ArcType> Destination {
			get {
				return destinationNode;
			}
		}
		
		/// <summary>
		/// Returns the weight.
		/// </summary>
		/// <value>
		/// The weight.
		/// </value>
		public ArcType Weight {
			get {
				return weight;
			}
		}
		
	}
	
}

