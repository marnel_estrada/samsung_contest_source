using System;
using System.Collections.Generic;

using Common.Utils;

namespace ShadowStar.Graph {
	
	/// <summary>
	/// Represents a node in a graph.
	/// </summary>
	public class GraphNode<NodeType, ArcType> {
		
		private NodeType nodeValue;
		private SimpleList<GraphArc<NodeType, ArcType>> arcList;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.Graph.GraphNode`2"/> class.
		/// </summary>
		/// <param name='nodeValue'>
		/// Node value.
		/// </param>
		public GraphNode(NodeType nodeValue) {
			this.nodeValue = nodeValue;
			this.arcList = new SimpleList<GraphArc<NodeType, ArcType>>();
		}
		
		/// <summary>
		/// Adds an arc.
		/// </summary>
		/// <param name='destination'>
		/// Destination.
		/// </param>
		/// <param name='weight'>
		/// Weight.
		/// </param>
		public void AddArc(GraphNode<NodeType, ArcType> destination, ArcType weight) {
			GraphArc<NodeType, ArcType> arc = new GraphArc<NodeType, ArcType>(destination, weight);
			arcList.Add(arc);
		}

		/// <summary>
		/// Returns the number of arcs
		/// </summary>
		/// <value>The arc count.</value>
		public int ArcCount {
			get {
				return this.arcList.Count;
			}
		}

		/// <summary>
		/// Returns the arc at the specified index
		/// </summary>
		/// <returns>The <see cref="ShadowStar.Graph.GraphArc`2[[`0],[`1]]"/>.</returns>
		/// <param name="index">Index.</param>
		public GraphArc<NodeType, ArcType> GetArcAt(int index) {
			return this.arcList[index];
		}
		
		/// <summary>
		/// Returns the value of the node.
		/// </summary>
		public NodeType Value {
			get {
				return nodeValue;
			}
		}
		
	}
	
}

