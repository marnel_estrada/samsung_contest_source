using System;
using System.Collections.Generic;

namespace ShadowStar.Utils {
	/// <summary>
	/// An object pool for BhNode instances.
	/// </summary>
	class BhNodePool<T> {
		
		private LinkedList<BhNode<T>> inactiveList;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.Utils.BhNodePool`1"/> class.
		/// </summary>
		public BhNodePool() {
			inactiveList = new LinkedList<BhNode<T>>();
		}
		
		/// <summary>
		/// Requests for a BhNode instance.
		/// </summary>
		public BhNode<T> Request(T nodeValue) {
			if(inactiveList.Count > 0) {
				// there are unused ones
				// we reuse them
				BhNode<T> instance = inactiveList.Last.Value;
				instance.Init(nodeValue);
				inactiveList.RemoveLast();
				return instance;
			}
			
			return new BhNode<T>(nodeValue);
		}
		
		/// <summary>
		/// Returns the specified instance to the pool.
		/// </summary>
		public void Return(BhNode<T> instance) {
			inactiveList.AddFirst(instance);
		}
	}
}
