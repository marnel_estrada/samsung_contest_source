using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ShadowStar.Utils {
	public class BinaryHeap<T> {
	
		private BinaryHeapComparator<T> comparator;
		private BhNode<T> root;
		private BhNodePool<T> nodePool;
	    
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.Utils.BinaryHeap`1"/> class.
		/// </summary>
		/// <param name='comparator'>
		/// Comparator.
		/// </param>
		public BinaryHeap(BinaryHeapComparator<T> comparator) {
			this.comparator = comparator;
			this.root = null;
			this.nodePool = new BhNodePool<T>();
		}
		
		/// <summary>
		/// Adds an element to the heap.
		/// </summary>
		public void Add(T element) {
			BhNode<T> newNode = nodePool.Request(element);
			
			if(root == null) {
				// means that this is the first element
				root = newNode;
				return;
			}
			
			BhNode<T> attachNode = ResolveNodeForAdding();
			Assertion.Assert(!(attachNode.HasLeft() && attachNode.HasRight())); // "At least one slot should be available for the new node."
			if(!attachNode.HasLeft()) {
				// no left, attach to left
				attachNode.AttachLeft(newNode);
			} else {
				// already has left
				// right should be empty at this point
				attachNode.AttachRight(newNode);
			}
			
			// fix heap
			BubbleUpNode(newNode);
		}

        private Queue<BhNode<T>> traversalQueue;

        /// <summary>
        /// Breadth first traversal
        /// This is useful for traversing from the least node which is at the top to the highest node at the bottom
        /// Note that this may not be in order
        /// </summary>
        /// <param name="visitor"></param>
        public void BreadthFirstTraversal(Func<T, bool> visitor) {
            if(IsEmpty()) {
                // There are no nodes
                // Nothing to traverse
                return;
            }

            if(this.traversalQueue == null) {
                // We lazy initialize because Breadth First search is not often used
                this.traversalQueue = new Queue<BhNode<T>>();
            }

            this.traversalQueue.Clear();

            this.traversalQueue.Enqueue(this.root);
            while(this.traversalQueue.Count > 0) {
                BhNode<T> node = this.traversalQueue.Dequeue();

                Assertion.AssertNotNull(node);

                // Visit the node
                if (visitor(node.Value)) {
                    // Visitor already returned true
                    // Stop the traversal
                    return;
                }

                // Enqueue children
                if (node.HasLeft() && node.HasRight()) {
                    // If node has both children, the one with lesser value will be enqueued first to preserve order
                    T leftValue = node.Left.Value;
                    T rightValue = node.Right.Value;

                    if(this.comparator.LesserThan(leftValue, rightValue)) {
                        // Left is lesser. Enqueue left first
                        this.traversalQueue.Enqueue(node.Left);
                        this.traversalQueue.Enqueue(node.Right);
                    } else {
                        // Right is lesser or equal. Enqueue right first nonetheless.
                        this.traversalQueue.Enqueue(node.Right);
                        this.traversalQueue.Enqueue(node.Left);
                    }
                } else {
                    if (node.HasLeft()) {
                        this.traversalQueue.Enqueue(node.Left);
                    }

                    if (node.HasRight()) {
                        this.traversalQueue.Enqueue(node.Right);
                    }
                }
            }
        }
		
		/// <summary>
		/// Resolves the next node where a new node should be attached.
		/// </summary>
		/// <returns>
		/// The attach node.
		/// </returns>
		private BhNode<T> ResolveNodeForAdding() {
			BhNode<T> current = root;
			
			while(current.HasLeft() && current.HasRight()) {
				// go to the node with lesser height to keep the tree balanced
				int leftHeight = current.Left.Height();
				int rightHeight = current.Right.Height();
				
				if(leftHeight < rightHeight) {
					current = current.Left;
				} else if(leftHeight == rightHeight) {
					// both have the same height
					// to keep a more balanced tree, we check first for a node which is unbalanced
					if(!current.Right.IsBalanced()) {
						current = current.Right;
					} else {
						current = current.Left; // this can be either balanced or not
					}
				} else {
					current = current.Right;
				}
			}
			
			return current;
		}
		
		/// <summary>
		/// Fixes the consistency by checking that the value of the specified node is greater than its parent.
		/// </summary>
		/// <param name='node'>
		/// Node.
		/// </param>
		private void BubbleUpNode(BhNode<T> node) {
			BhNode<T> current = node;
			
			// use iteration instead of recursion to save call stack
			while(true) {
				if(current.IsRoot()) {
					// already the root node
					break;
				}
				
				BhNode<T> parent = current.Parent;
				if(comparator.LesserThan(current.Value, parent.Value)) {
					// current node has lesser value than its parent, so we bubble up the value of the current node
					current.SwapValues(parent);
					current = parent;
				} else {
					// the value of the current node is already in its correct place
					break;
				}
			}
		}
		
		/// <summary>
		/// Returns whether or not the heap is empty.
		/// </summary>
		public bool IsEmpty() {
			return root == null;
		}
		
		/// <summary>
		/// Returns the root value.
		/// </summary>
		/// <value>
		/// The root value.
		/// </value>
		public T RootValue {
			get {
				if(root == null) {
					return default(T);
				}
				return root.Value;
			}
		}
		
		/// <summary>
		/// Removes the root. Returns the removed value.
		/// </summary>
		/// <returns>
		/// The removed value.
		/// </returns>
		public T RemoveRoot() {
			if(root == null) {
				return default(T);
			}
			
			T rootValue = RootValue;
			
			if(!(root.HasLeft() || root.HasRight())) {
				// root is the only node left
				// no need for replacement
				root = null;
				return rootValue;
			}
			
			// replace root node (not that we just swap the values to avoid clutter when swapping nodes themselves)
			BhNode<T> replacementNode = ResolveRootNodeReplacement();
			root.SwapValues(replacementNode);
			replacementNode.DetachFromParent();
			nodePool.Return(replacementNode);
			
			BubbleDownNode(root);
			
			return rootValue;
		}
		
		private void BubbleDownNode(BhNode<T> node) {
			BhNode<T> current = node;
			
			// use iteration instead of recursion to save call stack
			while(true) {
				if(!(current.HasLeft() || current.HasRight())) {
					// already a leaf node
					break;
				}
				
				// identify lesser of children
				BhNode<T> lesserChild = ResolveLesserChild(current);
				
				// check if need to bubble down
				if(comparator.LesserThan(lesserChild.Value, current.Value)) {
					// child is lesser than parent, need to swap values
					current.SwapValues(lesserChild);
					current = lesserChild;
				} else {
					// the value of the current node is already in its correct place
					break;
				}
			}
		}
		
		/// <summary>
		/// Resolves the lesser child of the specified node.
		/// </summary>
		/// <param name='node'>
		/// Node.
		/// </param>
		private BhNode<T> ResolveLesserChild(BhNode<T> node) {
			// should have at least one child
			Assertion.Assert(node.HasLeft() || node.HasRight()); // "Node should have at least one child"
			
			if(!node.HasLeft()) {
				// only have right node
				return node.Right;
			}
			
			if(!node.HasRight()) {
				// only have left node
				return node.Left;
			}
			
			// has two children at this point
			// needs comparison
			BhNode<T> lesserChild = node.Left;
			if(comparator.LesserThan(node.Right.Value, lesserChild.Value)) {
				lesserChild = node.Right;
			}
			
			return lesserChild;
		}
		
		/// <summary>
		/// Resolves the new node to replace the root during removal.
		/// </summary>
		/// <returns>
		/// The node that will replace the root node.
		/// </returns>
		public BhNode<T> ResolveRootNodeReplacement() {
			BhNode<T> current = root;
			
			while(current.HasLeft() || current.HasRight()) {
				if(!current.HasLeft()) {
					// only have right
					current = current.Right;
					continue;
				}
				
				if(!current.HasRight()) {
					// only has left
					current = current.Left;
					continue;
				}
				
				// go to the node with higher height to keep the tree balanced
				int leftHeight = current.Left.Height();
				int rightHeight = current.Right.Height();
				
				if(leftHeight > rightHeight) {
					current = current.Left;
				} else if(leftHeight == rightHeight) {
					// both have the same height
					// to keep a more balanced tree, we check first for a node which is balanced
					if(current.Right.IsBalanced()) {
						current = current.Right;
					} else {
						current = current.Left; // this can be either balanced or not
					}
				} else {
					current = current.Right;
				}
			}
			
			return current;
		}
		
		/// <summary>
		/// Clear the heap.
		/// </summary>
		public void Clear() {
			while(!IsEmpty()) {
				RemoveRoot();
			}
		}
		
		/// <summary>
		/// Fixes the heap from the specified element. The elements may have changed values so the heap must be restructured to still have a consistent heap state.
		/// </summary>
		/// <param name='element'>
		/// Element.
		/// </param>
		public void Fix(T element) {
			if(IsEmpty()) {
				return;
			}
			
			BhNode<T> nodeWithTheElement = FindNode(this.root, element);

            // The node may no longer be found or may have been removed
            if (nodeWithTheElement != null) {
                BubbleDownNode(nodeWithTheElement);
                BubbleUpNode(nodeWithTheElement);
            }
		}

        /// <summary>
        /// Fix specific nodes that satisfies the specified predicate
        /// </summary>
        /// <param name="predicate"></param>
        public void FixWhere(Func<T, bool> predicate) {
            if (!IsEmpty()) {
                // Check for not empty to prevent null pointer exception
                FixWhere(predicate, this.root);
            }
        }

        private void FixWhere(Func<T, bool> predicate, BhNode<T> node) {
            if(predicate(node.Value)) {
                BubbleDownNode(node);
                BubbleUpNode(node);
            }

            if(node.HasLeft()) {
                FixWhere(predicate, node.Left);
            }

            if(node.HasRight()) {
                FixWhere(predicate, node.Right);
            }
        }
		
		/// <summary>
		/// Looks for the node with the specified element.
		/// </summary>
		private BhNode<T> FindNode(BhNode<T> node, T element) {
			if(comparator.IsSameInstance(node.Value, element)) {
				return node;
			}
			
			if(node.HasLeft()) {
				BhNode<T> result = FindNode(node.Left, element);
				if(result != null) {
					return result;
				}
			}
			
			if(node.HasRight()) {
				BhNode<T> result = FindNode(node.Right, element);
				if(result != null) {
					return result;
				}
			}
			
			// node can't be found
			return null;
		}
		
		public override string ToString() {
			// empty heap
			if(IsEmpty()) {
				return "()";
			}
			
			StringBuilder builder = new StringBuilder();
			Report(root, builder);
			
			return builder.ToString();
		}
		
		private void Report(BhNode<T> node, StringBuilder builder) {
			// print node value
			builder.Append(node.Value.ToString()).Append('\n');
			
			if(!(node.HasLeft() || node.HasRight())) {
				// no children
				return;
			}
			
			// left
			int depth = node.Depth() + 1;
			for(int i = 0; i < depth; ++i) {
				builder.Append(' ').Append(' ');
			}
			
			if(node.HasLeft()) {
				Report(node.Left, builder);
			} else {
				builder.Append("()\n");
			}
			
			// right
			for(int i = 0; i < depth; ++i) {
				builder.Append(' ').Append(' ');
			}
			
			if(node.HasRight()) {
				Report(node.Right, builder);
			} else {
				builder.Append("()\n");
			}
		}
		
	}
	
}
