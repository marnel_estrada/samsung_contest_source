using System;

namespace ShadowStar.Utils {
	/// <summary>
	/// Comparator interface for the BinaryHeap.
	/// </summary>
	public interface BinaryHeapComparator<T> {
		
		/// <summary>
		/// Returns whether or not a is lesser than b.
		/// </summary>
		bool LesserThan(T a, T b);
		
		/// <summary>
		/// Returns whether or not a and b are of the same instance. We differentiate this from IsEqual() (used for equality but may be of different instances).
		/// </summary>
		bool IsSameInstance(T a, T b);
		
	}
}

