using System;

namespace ShadowStar.Utils {
	public class BhNode<T> {
		
		private BhNode<T> parent;
		private BhNode<T> left;
		private BhNode<T> right;
		
		private T nodeValue;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShadowStar.Utils.BhNode`1"/> class.
		/// </summary>
		public BhNode(T nodeValue) {
			Init(nodeValue);
		}
		
		/// <summary>
		/// Initializes the BinaryHeap node. We provide this method so we could implement an object pool for this class.
		/// </summary>
		public void Init(T nodeValue) {
			parent = null;
			left = null;
			right = null;
			
			this.nodeValue = nodeValue;
		}
		
		/// <summary>
		/// Swaps values with the specified node.
		/// </summary>
		/// <param name='other'>
		/// Other node.
		/// </param>
		public void SwapValues(BhNode<T> other) {
			T otherValue = other.Value;
			other.Value = this.nodeValue;
			this.nodeValue = otherValue;
		}
		
		public BhNode<T> Parent {
			get {
				return parent;
			}
		}
		
		public BhNode<T> Left {
			get {
				return left;
			}
		}
		
		public BhNode<T> Right {
			get {
				return right;
			}
		}
		
		public T Value {
			get {
				return nodeValue;
			}
			
			set {
				nodeValue = value;
			}
		}
		
		/// <summary>
		/// Returns whether or not the node is a root.
		/// </summary>
		/// <returns>
		/// <c>true</c> if this instance is root; otherwise, <c>false</c>.
		/// </returns>
		public bool IsRoot() {
			return parent == null;
		}
		
		/// <summary>
		/// Returns whether or not this node has a right node.
		/// </summary>
		/// <returns>
		/// <c>true</c> if this instance has a right node; otherwise, <c>false</c>.
		/// </returns>
		public bool HasRight() {
			return right != null;
		}
		
		/// <summary>
		/// Returns whether or not this node has a left node.
		/// </summary>
		/// <returns>
		/// <c>true</c> if this instance has a left node; otherwise, <c>false</c>.
		/// </returns>
		public bool HasLeft() {
			return left != null;
		}
		
		/// <summary>
		/// Returns whether or not the node has both a left and right child.
		/// </summary>
		/// <returns>
		/// <c>true</c> if this instance is full; otherwise, <c>false</c>.
		/// </returns>
		public bool IsFull() {
			return HasLeft() && HasRight();
		}
		
		/// <summary>
		/// Returns whether or not the node has an unpaired node in its heirarchy.
		/// </summary>
		public bool IsBalanced() {
			if(HasLeft() && HasRight()) {
				// return whichever is false
				return right.IsBalanced() && left.IsBalanced();
			}
			
			if(!(HasLeft() || HasRight())) {
				// no children, balanced
				return true;
			}
			
			// at this point, it is only has right or has left
			return false;
		}
		
		/// <summary>
		/// Returns the height of the node.
		/// </summary>
		public int Height() {
			int leftHeight = 0;
			if(left != null) {
				leftHeight = left.Height();
			}
			
			int rightHeight = 0;
			if(right != null) {
				rightHeight = right.Height();
			}
			
			return Math.Max(leftHeight, rightHeight) + 1;
		}
		
		/// <summary>
		/// Returns the depth of the node.
		/// </summary>
		public int Depth() {
			if(IsRoot()) {
				return 0;
			}
			
			return parent.Depth() + 1;
		}
		
		/// <summary>
		/// Attaches the specified node to its left.
		/// </summary>
		/// <param name='node'>
		/// BhNode.
		/// </param>
		public void AttachLeft(BhNode<T> node) {
			Assertion.Assert(node != this, "The specified node can't be itself.");
			this.left = node;
			node.parent = this;
		}
		
		/// <summary>
		/// Attaches the specified node to its right.
		/// </summary>
		/// <param name='node'>
		/// Node.
		/// </param>
		public void AttachRight(BhNode<T> node) {
			Assertion.Assert(node != this, "The specified node can't be itself.");
			this.right = node;
			node.parent = this;
		}
		
		/// <summary>
		/// Detaches the specified node.
		/// </summary>
		/// <param name='node'>
		/// Node.
		/// </param>
		public void DetachChild(BhNode<T> node) {
			Assertion.Assert(node == left || node == right, "The specified node should be either the left or right child node.");
			if(node == left) {
				// detach left
				left.parent = null;
				left = null;
			} else {
				// detach right
				right.parent = null;
				right = null;
			}
		}
		
		/// <summary>
		/// Detachess the node from its parent.
		/// </summary>
		public void DetachFromParent() {
			if(parent == null) {
				// no parent
				return;
			}
			
			parent.DetachChild(this);
		}
		
	}
	
}
