﻿using System;
using UnityEngine;
using Common.Logger;

public static class Assertion {

	private const string DEFAULT_MESSAGE = "AssertionError";

	/**
	 * Asserts the specified expression
	 */
	public static void Assert(bool expression, UnityEngine.Object context = null) {
		Assert(expression, DEFAULT_MESSAGE, context);
	}

	/**
     * Asserts the specified expression.
     */
	public static void Assert(bool expression, string assertErrorMessage, UnityEngine.Object context = null) {
		if (!expression) {
			Debug.LogError(assertErrorMessage, context);
			
			// use logger only if not in editor
#if !UNITY_EDITOR
			try {
#endif
				throw new Exception(assertErrorMessage);
#if !UNITY_EDITOR
			} catch(Exception e) {
				Common.Logger.Logger.GetInstance().LogError(assertErrorMessage);
				Common.Logger.Logger.GetInstance().LogError(e.StackTrace);
				throw e;
			}
#endif
        }
    }

	/**
     * Asserts that the specified pointer is not null.
     */
	public static void AssertNotNull(object pointer, string name, UnityEngine.Object context = null) {
		Assert(pointer != null, name + " should not be null", context);
	}

	/**
     * Asserts that the specified pointer is not null.
     */
	public static void AssertNotNull(object pointer, UnityEngine.Object context = null) {
		Assert(pointer != null, DEFAULT_MESSAGE, context);
	}
	
	/**
	 * Asserts that the specified UnityEngine object is not null.
	 */
	public static void AssertNotNull(UnityEngine.Object pointer, string name, UnityEngine.Object context = null) {
		if(!pointer) {
			Assert(false, name + " should not be null", context);
		}
	}

	/**
	 * Asserts that the specified UnityEngine object is not null.
	 */
	public static void AssertNotNull(UnityEngine.Object pointer, UnityEngine.Object context = null) {
		if(!pointer) {
			Assert(false, DEFAULT_MESSAGE, context);
		}
	}
	
	/**
	 * Asserts that the specified string is not empty.
	 */
	public static void AssertNotEmpty(string s, string name, UnityEngine.Object context = null) {
		Assert(!string.IsNullOrEmpty(s), name + " should not be empty", context);
	}

	
	/**
	 * Asserts that the specified string is not empty.
	 */
	public static void AssertNotEmpty(string s, UnityEngine.Object context = null) {
		Assert(!string.IsNullOrEmpty(s), DEFAULT_MESSAGE, context);
	}

}
