﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common {
    /// <summary>
    /// A utility class that handles next and previous functionality
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SelectionSequence<T> {

        private readonly List<T> items = new List<T>();

        private int currentIndex = 0;

        /// <summary>
        /// Default constructor
        /// </summary>
        public SelectionSequence() {
        }

        /// <summary>
        /// Constructor with specified array of items
        /// </summary>
        /// <param name="array"></param>
        public SelectionSequence(T[] array) {
            this.items.AddRange(array);
            Reset();
        }

        /// <summary>
        /// Resets the sequence
        /// </summary>
        public void Reset() {
            this.currentIndex = 0;
        }

        /// <summary>
        /// Moves to the next item
        /// </summary>
        public void MoveToNext() {
            this.currentIndex = (currentIndex + 1) % this.items.Count;
        }

        /// <summary>
        /// Moves to the previous item
        /// </summary>
        public void MoveToPrevious() {
            int decremented = this.currentIndex - 1;
            this.currentIndex = decremented < 0 ? this.items.Count - 1 : decremented;
        }

        /// <summary>
        /// Returns the current selected item
        /// </summary>
        public T Current {
            get {
                return this.items[this.currentIndex];
            }
        }

        /// <summary>
        /// Selects the current index
        /// </summary>
        /// <param name="index"></param>
        public void Select(int index) {
            this.currentIndex = index;
        }

        /// <summary>
        /// Selects the specified item
        /// </summary>
        /// <param name="item"></param>
        public void Select(T item) {
            for(int i = 0; i < this.items.Count; ++i) {
                if(this.items[i].Equals(item)) {
                    Select(i);
                    return;
                }
            }
            
            Assertion.Assert(false, "Can't find the item to select: " + item.ToString());
        }

        public int Count {
            get {
                return this.items.Count;
            }
        }

        /// <summary>
        /// Returns the item at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T GetAt(int index) {
            return this.items[index];
        }

    }
}
