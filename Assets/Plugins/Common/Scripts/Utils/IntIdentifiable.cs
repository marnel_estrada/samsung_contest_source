﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common {
    public interface IntIdentifiable {

        /// <summary>
        /// Int ID of the object
        /// </summary>
        int Id { get; set; }

    }
}
