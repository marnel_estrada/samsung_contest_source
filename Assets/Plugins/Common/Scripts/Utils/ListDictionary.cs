﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Common.Utils;

namespace Common {
    /// <summary>
    /// A container that manages items both in a list and dictionary
    /// </summary>
    public class ListDictionary<K, V> {

        private readonly List<K> keyList = new List<K>();
        private readonly List<V> valueList = new List<V>();
        private readonly Dictionary<K, V> dictionary = new Dictionary<K, V>();

        /// <summary>
        /// Constructor
        /// </summary>
        public ListDictionary() {
        }

        /// <summary>
        /// Adds an item
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(K key, V value) {
            Assertion.Assert(!this.dictionary.ContainsKey(key), key.ToString()); // should not have the said item in the container

            this.keyList.Add(key);
            this.valueList.Add(value);
            this.dictionary.Add(key, value);

            Assertion.Assert(this.keyList.Count == this.valueList.Count && this.valueList.Count == this.dictionary.Count);
        }

        /// <summary>
        /// Returns whether or not the container contains the specified key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(K key) {
            return this.dictionary.ContainsKey(key);
        }

        /// <summary>
        /// Removes an item with the specified key
        /// </summary>
        /// <param name="key"></param>
        public void Remove(K key) {
            int listIndex = this.keyList.IndexOf(key);
            this.keyList.RemoveAt(listIndex);
            this.valueList.RemoveAt(listIndex);

            this.dictionary.Remove(key);

            Assertion.Assert(this.keyList.Count == this.valueList.Count && this.valueList.Count == this.dictionary.Count);
        }

        /// <summary>
        /// Removes the item at the specified index
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index) {
            K key = this.keyList[index];
            this.keyList.RemoveAt(index);
            this.valueList.RemoveAt(index);
            this.dictionary.Remove(key);

            Assertion.Assert(this.keyList.Count == this.valueList.Count && this.valueList.Count == this.dictionary.Count);
        }

        /// <summary>
        /// Retrieves a value with the specified key
        /// Returns null if not found. Client code should check for this
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public V GetWithKey(K key) {
            V value = default(V);
            Assertion.Assert(this.dictionary.TryGetValue(key, out value), "Value not found for key " + key.ToString());
            return value;
        }

        /// <summary>
        /// Looks for the value with the specified key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public V Find(K key) {
            // This may be null so client code should check for it
            return this.dictionary.Find(key);
        }

        /// <summary>
        /// Retrieves a value using an index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public V GetAt(int index) {
            return this.valueList[index];
        }

        /// <summary>
        /// Clears the container of all items
        /// </summary>
        public void Clear() {
            this.keyList.Clear();
            this.valueList.Clear();
            this.dictionary.Clear();

            Assertion.Assert(this.keyList.Count == this.valueList.Count && this.valueList.Count == this.dictionary.Count);
        }

        /// <summary>
        /// Returns the number of items in the container
        /// </summary>
        public int Count {
            get {
                Assertion.Assert(this.keyList.Count == this.valueList.Count && this.valueList.Count == this.dictionary.Count);
                return this.valueList.Count;
            }
        }

        public V[] Values {
            get {
                return this.valueList.ToArray();
            }
        }

        public IEnumerable<KeyValuePair<K, V>> KeyValueEntries {
            get {
                return this.dictionary;
            }
        }

    }
}
